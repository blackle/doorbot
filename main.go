package main

import (
	"fmt"
	"io"
	"log/syslog"
	"os"
	"os/signal"
	"syscall"

	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
)

func main() {
	// gets common CLI flags
	debugFlag, traceFlag, fakeNFCFlag, configFlag := config.GetCommandFlags()

	// get the config specified in the settings.conf file
	cfg, err := config.Get(*configFlag)
	if err != nil {
		panic(fmt.Errorf("could not get config from path: %s (%w)", *configFlag, err))
	}

	// set up the logging
	logger, err := setupLogging(cfg, *debugFlag, *traceFlag)
	if err != nil {
		panic(fmt.Errorf("failed to set up logger: %w", err))
	}

	// get the config specified in the settings.conf file
	secrets, err := config.GetSecrets(cfg.Paths.SecretsFile)
	if err != nil {
		logger.Fatal().Str("path", cfg.Paths.SecretsFile).Err(err).Msg("failed to read config file")
	}

	// doorbot contains everything that doorbot actually does
	doorbot, err := NewDoorbot(&logger, cfg, secrets, *fakeNFCFlag)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize doorbot")
	}

	// start the doorbot in a goroutine
	go doorbot.Start()

	// sets up a channel to receive signals from the OS to stop the process
	exitChan := make(chan os.Signal)
	signal.Notify(exitChan, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGKILL)

	// this waits for the channel to receive a signal (in this case, a SIG* or Interrupt)
	// it continues onwards once the signal has been received
	<-exitChan

	// after the kill command has been received, stop everything
	doorbot.Stop()

	logger.Info().Msg("THANK YOU FOR USING DOORBOT, HAVE A WONDERFUL DAY")
}

// setupLogging sets up zerolog with various log writers
func setupLogging(cfg *config.DoorbotConfig, debugFlag, traceFlag bool) (zerolog.Logger, error) {
	// consoleWriter outputs to the raw Stdout console
	consoleWriter := zerolog.ConsoleWriter{
		Out:        os.Stdout,
		TimeFormat: "15:04:05 2006-01-01",
	}

	// fileWriter writes to a local file
	var err error
	var fileWriter io.Writer
	if cfg.Logging.LocalFileEnabled {
		fileWriter, err = os.OpenFile(cfg.Logging.LocalFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0660)
		if err != nil {
			panic(fmt.Errorf("could not open log file at path: %s", cfg.Logging.LocalFilePath))
		}
	}

	// remoteWriter writes to a remote syslog instance
	var remoteWriter zerolog.SyslogWriter
	if cfg.Logging.RemoteEnabled {
		remoteWriter, err = syslog.Dial("tcp", cfg.Logging.RemoteAddr, syslog.LOG_WARNING|syslog.LOG_DAEMON, "doorbot")
		if err != nil {
			panic(fmt.Errorf("remote syslog enabled, but failed to establish connection to %s: %w", cfg.Logging.RemoteAddr, err))

		}
	}

	// define the outputs, depending on how they're configured
	// I wish you could simply append new writers to the end of an array but you can't do that here, sigh
	outputs := zerolog.MultiLevelWriter(consoleWriter)
	if cfg.Logging.RemoteEnabled && cfg.Logging.LocalFileEnabled {
		outputs = zerolog.MultiLevelWriter(consoleWriter, fileWriter, remoteWriter)
	} else if cfg.Logging.RemoteEnabled {
		outputs = zerolog.MultiLevelWriter(consoleWriter, remoteWriter)
	} else if cfg.Logging.LocalFileEnabled {
		outputs = zerolog.MultiLevelWriter(consoleWriter, fileWriter)
	}

	// create the new logger with outputs, and append the version string to every log line
	logger := zerolog.New(outputs).
		With().
		Timestamp().
		Str("version", config.GetVersion()).
		Logger().
		Level(zerolog.InfoLevel)

	logger.Info().Msg("DOORBOT STARTING")

	if debugFlag || cfg.Logging.Debug {
		logger = logger.With().Stack().Logger().Level(zerolog.DebugLevel)
		logger.Debug().Msg("debugging logging enabled")
	}
	if traceFlag || cfg.Logging.Trace {
		logger = logger.With().Stack().Logger().Level(zerolog.TraceLevel)
		logger.Trace().Msg("trace logging enabled")
	}

	return logger, nil
}
