package devices

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/gpio/gpioreg"
	"periph.io/x/host/v3"
)

// DoorStrike is the struct that contains everything necessary for managing the door system
type DoorStrike struct {
	logger  zerolog.Logger
	context context.Context

	timerChan chan time.Duration // total time the door should remain open when card is read

	strikeChan chan bool  // for controlling the state of the strike (open/closed)
	strikePin  gpio.PinIO // pin for controlling the relay that controls the door strike
}

// NewDoorStrike sets up the doorstrike module and associated GPIO pins
func NewDoorStrike(ctx context.Context, l zerolog.Logger, config *config.DoorstrikeConfig) (*DoorStrike, error) {
	logger := l.With().Str("component", "doorstrike").Logger()

	host.Init()

	// initialize pins by name
	strikePin := config.StrikePin
	strikePinGPIO := gpioreg.ByName(strikePin)
	if strikePinGPIO == nil {
		logger.Fatal().Str("pin", strikePin).Msg("could not determine strike pin by name")
	}

	ds := &DoorStrike{
		logger:     logger,
		context:    ctx,
		strikePin:  strikePinGPIO,
		timerChan:  make(chan time.Duration),
		strikeChan: make(chan bool, 10),
	}

	logger.Info().Msg("initialized door strike device")

	return ds, nil
}

// Start begins
func (ds *DoorStrike) Start(waitGroup *sync.WaitGroup) {
	ds.logger.Debug().Msg("starting door strike")
	go ds.timerLoop(waitGroup)
	go ds.strikeLoop(waitGroup)
	ds.logger.Info().Msg("started door strike")
}

// Unlock is the external door unlocking caller
func (ds *DoorStrike) UnlockForDuration(duration time.Duration) error {
	ds.logger.Debug().Dur("duration", duration).Msg("set timer")
	ds.timerChan <- duration
	return nil
}

// timerLoop controls how long the door should stay open for during normal operation
// it uses a channel (ds.timerChan) to read how long the door should stay open for
// which is typically set by UnlockForDuration()
func (ds *DoorStrike) timerLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	timerStarted := false
	timer := time.NewTimer(0 * time.Millisecond)
	<-timer.C

	ds.logger.Debug().Msg("starting door timer loop")

	for {
		select {
		case duration := <-ds.timerChan: // set the timer
			ds.logger.Debug().Str("duration", duration.String()).Msg("timer set")
			if timerStarted {
				timer.Reset(duration)
				continue
			}

			timer = time.NewTimer(duration)

			ds.strikeChan <- true
			timerStarted = true

		case <-timer.C: // if the timer runs out
			ds.strikeChan <- false
			timerStarted = false

		case <-ds.context.Done():
			ds.logger.Info().Msg("stopped door timer controller")
			waitGroup.Done()
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}

// strikeLoop controls whether the relay for the door strike should be turned on
func (ds *DoorStrike) strikeLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	ds.logger.Debug().Msg("starting door strike controller loop")

	for {
		select {
		case open := <-ds.strikeChan:
			if open {
				err := ds.UnlockDoor()
				if err != nil {
					ds.logger.Error().Err(err).Msg("failed to unlock door")
				}
				continue
			}

			err := ds.LockDoor()
			if err != nil {
				ds.logger.Error().Err(err).Msg("failed to lock door")
			}

		case <-ds.context.Done():
			ds.logger.Debug().Msg("stopping strike controller")
			ds.LockDoor() // lock the door before stopping the pin
			// halt the pin to free it up in the driver
			if err := ds.strikePin.Halt(); err != nil {
				ds.logger.Error().Err(err).Msg("could not halt strike relay pin")
			}
			waitGroup.Done()
			ds.logger.Info().Msg("stopped strike controller")
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}

// LockDoor is a wrapper function to lock the door
func (ds *DoorStrike) LockDoor() error {
	ds.logger.Info().Msg("locking door")
	return ds.strikePin.Out(gpio.Low)
}

// UnlockDoor is a wrapper function to unlock the door
func (ds *DoorStrike) UnlockDoor() error {
	ds.logger.Info().Msg("unlocking door")
	return ds.strikePin.Out(gpio.High)
}
