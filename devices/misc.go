package devices

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/gpio/gpioreg"
	"periph.io/x/host/v3"
)

const (
	miscComponentName   = "misc"
	defaultEdgeWaitTime = time.Duration(300 * time.Millisecond)
)

// MiscController controls other smaller devices, such as the motion sensor and doorbell
type MiscController struct {
	logger  zerolog.Logger
	context context.Context
	config  *config.MiscConfig

	// the motion detector is a small motion sensor module with a single pin that gets pulled HIGH when motion is detected
	MotionChan     chan bool
	MotionNewEvent bool
	motionPin      gpio.PinIO
	motionTime     int

	// the doorbell is a button switch that gets pulled HIGH when pressed
	DoorbellChan chan bool
	doorbellPin  gpio.PinIO

	// the door opener is a hall effect sensor that pulls the pin HIGH when the door is open
	DoorOpenNotificationChan chan bool
	doorOpenPin              gpio.PinIO
	doorOpenLastState        gpio.Level
	DoorOpenTime             int

	// the event mode switch that keeps the door open
	EventModeChan    chan bool  // whether or not the door should be kept open or not (via a switch)
	EventModeEnabled bool       // for controlling whether or not to skip other checks
	eventModePin     gpio.PinIO // pin for detecting when the door event mode is engaged

}

// NewMiscController sets up a controller for small devices like the motion sensor or doorbell
func NewMiscController(ctx context.Context, l zerolog.Logger, cfg *config.MiscConfig) (*MiscController, error) {
	logger := l.With().Str("component", miscComponentName).Logger()

	host.Init()

	// setting up all the pins
	motionPin := cfg.MotionPin
	doorbellPin := cfg.DoorbellPin
	doorOpenPin := cfg.DoorOpenPin
	eventModePin := cfg.EventModePin

	motionPinGPIO := gpioreg.ByName(motionPin)
	if motionPinGPIO == nil {
		logger.Fatal().Str("pin", motionPin).Msg("could not determine motion pin by name")
	}

	doorbellPinGPIO := gpioreg.ByName(doorbellPin)
	if doorbellPinGPIO == nil {
		logger.Fatal().Str("pin", doorbellPin).Msg("could not determine doorbell pin by name")
	}

	doorOpenPinGPIO := gpioreg.ByName(doorOpenPin)
	if doorOpenPinGPIO == nil {
		logger.Fatal().Str("pin", doorOpenPin).Msg("could not determine door open detector pin by name")
	}

	eventModePinGPIO := gpioreg.ByName(eventModePin)
	if eventModePinGPIO == nil {
		logger.Fatal().Str("pin", eventModePin).Msg("could not determine event mode pin by name")
	}

	// "RisingEdge" here means the code in miscLoop will detect when the pin is going from low->high
	// but not when high->low
	// this is what we want because we don't really care that a button isn't being pressed, when motion isn't detected, etc.
	if err := motionPinGPIO.In(gpio.PullDown, gpio.RisingEdge); err != nil {
		logger.Error().Err(err).Msg("could not set input for motion pin")
	}

	if err := doorbellPinGPIO.In(gpio.PullDown, gpio.RisingEdge); err != nil {
		logger.Error().Err(err).Msg("could not set input for doorbell pin")
	}

	if err := doorOpenPinGPIO.In(gpio.PullDown, gpio.BothEdges); err != nil {
		logger.Error().Err(err).Msg("could not set input for doorOpen pin")
	}

	if err := eventModePinGPIO.In(gpio.PullDown, gpio.BothEdges); err != nil {
		logger.Error().Err(err).Msg("could not set input for eventMode pin")
	}

	misc := &MiscController{
		logger:                   logger,
		context:                  ctx,
		config:                   cfg,
		motionPin:                motionPinGPIO,
		MotionChan:               make(chan bool),
		MotionNewEvent:           true,
		motionTime:               cfg.MotionTime,
		doorbellPin:              doorbellPinGPIO,
		DoorbellChan:             make(chan bool),
		doorOpenPin:              doorOpenPinGPIO,
		DoorOpenNotificationChan: make(chan bool),
		doorOpenLastState:        gpio.Low,
		DoorOpenTime:             cfg.DoorOpenTime,
		eventModePin:             eventModePinGPIO,
		EventModeChan:            make(chan bool, 2),
	}

	logger.Info().Msg("initialized misc device controller")

	return misc, nil
}

// Start begins the loop that accepts commanmisc.for the misc devices
func (misc *MiscController) Start(waitGroup *sync.WaitGroup) {
	misc.logger.Debug().Msg("starting misc controller")

	if misc.eventModePin.Read() == gpio.High {
		misc.logger.Debug().Msg("set eventMode true on boot")
		misc.EventModeChan <- true
	}

	go misc.miscLoop(waitGroup)

	misc.logger.Info().Msg("started misc controller")
}

// main misc loop
func (misc *MiscController) miscLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	misc.logger.Debug().Msg("beginning misc loop")
	// new timer to determine how long the door has been opened
	doorOpenTimer := time.NewTimer(0 * time.Millisecond)
	motionTimer := time.NewTimer(0 * time.Millisecond)

	// flush the timer first
	<-doorOpenTimer.C
	<-motionTimer.C

	for {
		motionDetected := misc.motionPin.WaitForEdge(defaultEdgeWaitTime)
		if motionDetected {
			currentRead := misc.motionPin.Read()
			misc.logger.Trace().Str("state", currentRead.String()).Msg("detected motion event")
			if currentRead == gpio.High {
				misc.MotionChan <- true
				motionTimer = time.NewTimer(time.Duration(misc.motionTime) * time.Second)
			}
		}

		doorbellDetected := misc.doorbellPin.WaitForEdge(defaultEdgeWaitTime)
		if doorbellDetected {
			currentRead := misc.doorbellPin.Read()
			misc.logger.Trace().Str("state", currentRead.String()).Msg("detected doorbell event")
			if currentRead == gpio.High {
				misc.logger.Debug().Str("state", currentRead.String()).Msg("sending doorbell event")
				misc.DoorbellChan <- true
				time.Sleep(500 * time.Millisecond)
			}
		}

		doorOpenDetected := misc.doorOpenPin.WaitForEdge(defaultEdgeWaitTime)
		// we only care if the state has changed
		if doorOpenDetected && misc.config.DoorOpenEnabled {
			currentRead := misc.doorOpenPin.Read()
			misc.logger.Debug().Str("state", currentRead.String()).Msg("detected door open event")
			if currentRead != misc.doorOpenLastState && currentRead == gpio.Low {
				doorOpenTimer = time.NewTimer(time.Duration(misc.DoorOpenTime) * time.Minute)
			} else if currentRead == gpio.High {
				// stop the timer since the door has closed
				doorOpenTimer.Stop()
			}
			// save the last state
			misc.doorOpenLastState = currentRead
		} else if !misc.config.DoorOpenEnabled {
			doorOpenTimer.Stop()
		}

		eventModeDetected := misc.eventModePin.WaitForEdge(defaultEdgeWaitTime)
		if eventModeDetected {
			currentRead := misc.eventModePin.Read()
			misc.logger.Debug().Str("state", currentRead.String()).Msg("detected eventMode event")
			if currentRead == gpio.High {
				misc.EventModeChan <- true
			} else {
				misc.EventModeChan <- false
			}
		}

		select {
		case <-doorOpenTimer.C:
			// notify that the door has remained open far longer than expected
			misc.logger.Debug().Msg("door open timer expired; sending notification signal")
			misc.DoorOpenNotificationChan <- true
			// reset the timer
			doorOpenTimer = time.NewTimer(time.Duration(misc.DoorOpenTime) * time.Minute)

		case <-motionTimer.C:
			// this is to turn the LEDs on the door off if no one is nearby
			currentRead := misc.motionPin.Read()
			if currentRead == gpio.Low {
				misc.logger.Debug().Msg("motion timer expired; sending notification signal")
				misc.MotionChan <- false
			} else {
				motionTimer = time.NewTimer(time.Duration(misc.motionTime) * time.Second)
			}

		case <-misc.context.Done():
			misc.logger.Debug().Msg("stopped misc controller")
			if err := misc.doorOpenPin.Halt(); err != nil {
				misc.logger.Error().Err(err).Msg("could not halt door open pin")
			}
			if err := misc.doorbellPin.Halt(); err != nil {
				misc.logger.Error().Err(err).Msg("could not halt door bell pin")
			}
			if err := misc.motionPin.Halt(); err != nil {
				misc.logger.Error().Err(err).Msg("could not halt motion sensor pin")
			}
			if err := misc.eventModePin.Halt(); err != nil {
				misc.logger.Error().Err(err).Msg("could not halt eventMode switch pin")
			}
			misc.logger.Info().Msg("stopped misc controller")
			waitGroup.Done()
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}
