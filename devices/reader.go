package devices

import (
	"context"
	"encoding/hex"
	"fmt"
	"sync"
	"time"

	nfc "github.com/StarWitch/nfc/v2"
	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
	"periph.io/x/host/v3"
)

const (
	componentName = "reader" // for logging

	// EXPLANATION (READ THIS)
	// these two flags probably don't make sense at first glance
	// but the first flag (a blank string: "") is the CORRECT one
	// see, a blank string makes the library use sensible defaults and autodetection, which actually work
	// but if you specify the driver name, it stops working because it expects a bunch of other configuration that isn't being done
	// however, the driver can initialize without communicating to an actual chip
	// this can be useful for testing when you don't care about the reader being alive
	//
	// tl;dr: "" == good, "pn71xx" == no work
	readerAutoConfigFlag = ""       // an empty string here makes libnfc-nci auto-detect the reader, which works better than trying to manually specify it
	readerFakeConfigFlag = "pn71xx" // defining the actual driver makes the whole thing fail... don't ask me, I didn't write that library
)

// Reader struct contains everything that's needed to manage the card reader device
type Reader struct {
	logger  zerolog.Logger
	context context.Context

	device    *nfc.Device      // the actual device
	cardTypes []nfc.Modulation // contains a list of allowable card types

	nfcConfigFlag string // define what mode the driver should use

	DetectedID chan config.PhysicalCard // channel used to pass around which ID was just scanned (i.e: to the doorstrike thread)
}

// ReaderStart initializes a new NFC reader device
func ReaderStart(readerConfigFlag string) (*nfc.Device, error) {
	// open the GPIO device through I2C
	dev, err := nfc.Open(readerConfigFlag)
	if err != nil {
		return nil, fmt.Errorf("could not open reader device: %v", err)
	}

	// start the device
	if err = dev.InitiatorInit(); err != nil {
		return nil, fmt.Errorf("could not initialize reader device: %v", err)
	}
	return &dev, err
}

// NewCardReader sets up the card reader, including necessary GPIO settings
func NewCardReader(ctx context.Context, l zerolog.Logger, fakeNFCFlag bool) (*Reader, error) {
	l.Trace().Msg("starting NewCardReader")

	host.Init()

	nfcFlag := readerAutoConfigFlag
	if fakeNFCFlag {
		nfcFlag = readerFakeConfigFlag
	}

	dev, err := ReaderStart(nfcFlag)
	if err != nil {
		return nil, err
	}

	connection := dev.Connection()
	logger := l.With().Str("component", componentName).Str("driver", connection).Logger()

	// print optional information - it's OK if this fails (kinda)
	info, err := dev.Information()
	if err != nil {
		logger.Debug().Err(err).Msg("could not get device information")
	} else {
		logger.Debug().Str("info", info).Msg("retrieved reader device info")
	}

	// common card types, more can be added
	cardTypes := []nfc.Modulation{
		{Type: nfc.ISO14443a, BaudRate: nfc.Nbr106},
		{Type: nfc.ISO14443b, BaudRate: nfc.Nbr106},
		{Type: nfc.Felica, BaudRate: nfc.Nbr212},
		{Type: nfc.Felica, BaudRate: nfc.Nbr424},
		{Type: nfc.Jewel, BaudRate: nfc.Nbr106},
		{Type: nfc.ISO14443biClass, BaudRate: nfc.Nbr106},
	}

	// allows a detected card to be passed up to other threads (i.e: doorstrike, entry doodads, etc)
	idChan := make(chan config.PhysicalCard, 2)

	// create new reader device
	reader := &Reader{
		logger:        logger,
		context:       ctx,
		device:        dev,
		nfcConfigFlag: nfcFlag,
		cardTypes:     cardTypes,
		DetectedID:    idChan,
	}

	logger.Info().Msg("initialized reader device")

	return reader, nil
}

// Start begins the main reader loop
func (r *Reader) Start(waitGroup *sync.WaitGroup) {
	r.logger.Debug().Msg("starting card reader")
	go r.readerLoop(waitGroup)
	r.logger.Info().Msg("started card reader")
}

// readerLoop continuously polls the reader device to see if a card has been presented
// it then goes through a configured list of card types to see if any of those types have matched
// if so, if passes the card details up through a channel to other threads
func (r *Reader) readerLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	r.logger.Info().Msg("listening for cards")
	// we want the main `case` to run every 100 milliseconds
	timer := time.NewTicker(100 * time.Millisecond)
	for {
		select {
		case <-timer.C:
			// poll the device for card activations
			// my understanding is that this effectively tells the reader to coconcurrently check five times in 300 milliseconds
			// I could be wrong
			tagCount, target, err := r.device.InitiatorPollTarget(r.cardTypes, 5, 300*time.Millisecond)
			if err != nil {
				r.logger.Error().Err(err).Msg("failed to poll for cards")
				continue
			}

			if tagCount <= 0 {
				continue
			}

			r.logger.Debug().Int("tags", tagCount).Msg("tags found")
			physicalCard := &config.PhysicalCard{}

			// this block assumes we found a card, and goes through all the supported card types to see which one we found
			// it then translates the card data into a format we can pass on to other goroutines
			switch target.Modulation() {
			case nfc.Modulation{Type: nfc.ISO14443a, BaudRate: nfc.Nbr106}:
				var received = target.(*nfc.ISO14443aTarget)
				var UIDLen = received.UIDLen
				var ID = received.UID
				physicalCard.ID = hex.EncodeToString(ID[:UIDLen])
				physicalCard.Type = nfc.ISO14443a
				physicalCard.BaudRate = nfc.Nbr106
				r.logger.Debug().Str("id", hex.EncodeToString(ID[:UIDLen])).Str("string", received.String()).Msg("data received (ISO14443a)")
				break
			case nfc.Modulation{Type: nfc.ISO14443b, BaudRate: nfc.Nbr106}:
				var received = target.(*nfc.ISO14443bTarget)
				var UIDLen = len(received.ApplicationData)
				var ID = received.ApplicationData
				physicalCard.ID = hex.EncodeToString(ID[:UIDLen])
				physicalCard.Type = nfc.ISO14443b
				physicalCard.BaudRate = nfc.Nbr106
				r.logger.Debug().Str("id", physicalCard.ID).Str("string", received.String()).Msg("data received (ISO14443b)")
				break
			case nfc.Modulation{Type: nfc.Felica, BaudRate: nfc.Nbr212}:
				var received = target.(*nfc.FelicaTarget)
				var UIDLen = received.Len
				var ID = received.ID
				physicalCard.ID = hex.EncodeToString(ID[:UIDLen])
				physicalCard.Type = nfc.Felica
				physicalCard.BaudRate = nfc.Nbr212
				r.logger.Debug().Str("id", physicalCard.ID).Str("string", received.String()).Msg("data received (Felica (nbr: 212))")
				break
			case nfc.Modulation{Type: nfc.Felica, BaudRate: nfc.Nbr424}:
				var received = target.(*nfc.FelicaTarget)
				var UIDLen = received.Len
				var ID = received.ID
				physicalCard.ID = hex.EncodeToString(ID[:UIDLen])
				physicalCard.Type = nfc.Felica
				physicalCard.BaudRate = nfc.Nbr424
				r.logger.Debug().Str("id", physicalCard.ID).Str("string", received.String()).Msg("data received (Felica (nbr: 242))")
				break
			case nfc.Modulation{Type: nfc.Jewel, BaudRate: nfc.Nbr106}:
				var received = target.(*nfc.JewelTarget)
				var ID = received.ID
				var UIDLen = len(ID)
				physicalCard.ID = hex.EncodeToString(ID[:UIDLen])
				physicalCard.Type = nfc.Jewel
				physicalCard.BaudRate = nfc.Nbr106
				r.logger.Debug().Str("id", physicalCard.ID).Str("string", received.String()).Msg("data received (Jewel)")
				break
			case nfc.Modulation{Type: nfc.ISO14443biClass, BaudRate: nfc.Nbr106}:
				var received = target.(*nfc.ISO14443biClassTarget)
				var ID = received.UID
				var UIDLen = len(ID)
				physicalCard.ID = hex.EncodeToString(ID[:UIDLen])
				physicalCard.Type = nfc.ISO14443biClass
				physicalCard.BaudRate = nfc.Nbr106
				r.logger.Debug().Str("id", physicalCard.ID).Str("string", received.String()).Msg("data received (ISO14443biClass)")
				break
			}

			// pass physical card details up through a channel to unlock door, etc.
			if physicalCard.ID != "" {
				r.logger.Debug().Str("ID", physicalCard.ID).
					Int("type", physicalCard.Type).
					Int("baudrate", physicalCard.BaudRate).
					Msg("detected card; forwarding to channel")
				r.DetectedID <- *physicalCard
			}

			// FIXME: this sucks and I hate it
			// for some reason, the reader holds onto the previously read card unless we completely reinitialize the connection
			// resetting the connection here after every successful card read fixes it
			// we'll see if this causes any major problems down the line
			// could be a library problem... idk
			r.logger.Trace().Msg("closing card reader")

			err = r.device.Close()
			if err != nil {
				r.logger.Error().Err(err).Msg("could not close device")
			}

			r.logger.Trace().Msg("re-initializing card reader")

			r.device, err = ReaderStart(r.nfcConfigFlag)
			if err != nil {
				r.logger.Error().Err(err).Msg("could not open device")
			}

			r.logger.Trace().Msg("reader re-initialized; waiting for 1500 milliseconds")

			// wait for a while before doing that again
			time.Sleep(1500 * time.Millisecond)

		case <-r.context.Done():
			r.logger.Debug().Msg("stopping card reader controller")
			r.device.Close()
			r.logger.Info().Msg("stopped card reader controller")
			waitGroup.Done()
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}
