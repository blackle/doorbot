package devices

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"github.com/stianeikeland/go-rpio/v4"
	"gitlab.com/queer-computer-club/doorbot/config"
)

// code in this package taken mostly from: https://github.com/a-h/beeper

type NoteLength float64
type NoteFreq float32
type ChimeID int

const (
	// this is the list of chimes currently available
	// add new ones here
	DefaultAcceptChime ChimeID = iota
	DefaultDenyChime
	DefaultDoorBell

	// definitions for common note intervals
	WholeNote     NoteLength = HalfNote * 2
	HalfNote      NoteLength = QuarterNote * 2
	QuarterNote   NoteLength = 1.0
	EighthNote    NoteLength = QuarterNote / 2.0
	SixteenthNote NoteLength = EighthNote / 2.0
	InvalidNote   NoteLength = 0.0

	// for logging
	buzzerComponentName = "buzzer"
)

// Buzzer defines everything needed to utilize the buzzer in the card reader
type Buzzer struct {
	logger  zerolog.Logger
	context context.Context
	config  *config.BuzzerConfig

	songs        *config.SongsDB
	pin          rpio.Pin
	playSongChan chan ChimeID
}

// NewBuzzerController sets up the buzzer module and initializes the Songs database
func NewBuzzerController(ctx context.Context, l zerolog.Logger, cfg *config.DoorbotConfig) (*Buzzer, error) {
	logger := l.With().Str("component", buzzerComponentName).Int("pin", cfg.Buzzer.Pin).Logger()

	songsDB, err := config.GetSongsDB(cfg.Paths.SongsDBFile)
	if err != nil {
		return nil, fmt.Errorf("could not get songs DB: %v", err)
	}

	err = rpio.Open()
	if err != nil {
		return nil, fmt.Errorf("could not open gpio")
	}

	pin := rpio.Pin(cfg.Buzzer.Pin)
	pin.Mode(rpio.Pwm)

	buzzer := &Buzzer{
		logger:       logger,
		context:      ctx,
		config:       &cfg.Buzzer,
		pin:          pin,
		songs:        songsDB,
		playSongChan: make(chan ChimeID),
	}

	logger.Info().Msg("initialized buzzer device")

	return buzzer, nil
}

// Start begins the buzzer loop
func (b *Buzzer) Start(waitGroup *sync.WaitGroup) {
	b.logger.Debug().Msg("starting buzzer")
	go b.playLoop(waitGroup)
	b.logger.Info().Msg("started buzzer")
}

// playLoop handles play requests for the door chime
func (b *Buzzer) playLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	b.logger.Debug().Msg("beginning buzzer loop")

	for {
		select {
		case songID := <-b.playSongChan:
			b.play(songID)

		case <-b.context.Done():
			b.logger.Debug().Msg("stopping buzzer controller")
			b.ClearPWM() // make the buzzer stop buzzing (hopefully)
			rpio.Close()
			b.logger.Info().Msg("stopped buzzer controller")
			waitGroup.Done()
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}

// PlayChime plays a chime based on the number in songs.conf
func (b *Buzzer) PlayChime(id ChimeID) {
	if !b.config.Enabled {
		b.logger.Info().Msg("buzzer disabled; skipping")
		return
	}
	b.playSongChan <- id
}

// getNote returns a PWM frequency and duration based on the note name, length, and beats per minute
func (b *Buzzer) getNote(name config.NoteName, length NoteLength, bpm int) (NoteFreq, time.Duration, error) {
	crotchet := float64(time.Minute) / float64(bpm)
	freq, ok := notes[name]
	if !ok {
		return 0, 0, fmt.Errorf("could not find note: %s", name)
	}
	dur := time.Duration(crotchet * float64(length))
	return freq, dur, nil
}

// rest initiates a time-off/sleep based on its length and the BPM
func (b *Buzzer) rest(length NoteLength, bpm int) {
	crotchet := float64(time.Minute) / float64(bpm)
	dur := time.Duration(crotchet * float64(length))
	time.Sleep(dur)
}

// playNote makes the buzzer play the actual note using PWM into the buzzer
func (b *Buzzer) playNote(freq NoteFreq, duration time.Duration) {
	b.logger.Debug().Int("freq", int(freq)).Int("dur", int(duration)).Msg("playing note")
	modifier := 512
	b.pin.Freq(int(freq) * modifier)
	b.pin.DutyCycle(32, uint32(modifier))
	rpio.StartPwm()
	time.Sleep(duration)
	rpio.StopPwm()
}

// ClearPWM ensures the buzzer turns off when the bot shuts off
func (b *Buzzer) ClearPWM() {
	b.pin.Freq(1)
	b.pin.DutyCycle(0, 32)
	rpio.StopPwm()
}

// selectDurationFromShorthand takes the shorthand words for note names and returns a NoteLength
func selectDurationFromShorthand(duration string) (NoteLength, error) {
	switch duration {
	case config.WholeNoteName:
		return WholeNote, nil
	case config.HalfNoteName:
		return HalfNote, nil
	case config.QuarterNoteName:
		return QuarterNote, nil
	case config.EighthNoteName:
		return EighthNote, nil
	case config.SixteenthNoteName:
		return SixteenthNote, nil
	default:
		return InvalidNote, fmt.Errorf("invalid note length: %s", duration)
	}
}

// play gets a song from a ChimeID, and plays each note in the song
func (b *Buzzer) play(songID ChimeID) {
	song, ok := b.songs.Songs[int(songID)]
	if !ok {
		b.logger.Error().Msg("could not get song")
	}

	b.logger.Info().Int("song-id", int(songID)).Str("song-name", song.Name).Msg("playing song")
	for i := 0; i < len(song.Melody); i++ {
		note, ok := song.Melody[i]
		if !ok {
			b.logger.Warn().Int("note-num", i).Msg("error playing melody; note not found")
			continue
		}

		noteDuration, err := selectDurationFromShorthand(note.Duration)
		if err != nil {
			b.logger.Warn().Err(err).Int("note-num", i).Msg("error playing melody")
			continue
		}

		if note.Tone == "Rest" {
			b.rest(noteDuration, song.BPM)
			continue
		}

		b.ClearPWM()

		pitch, length, err := b.getNote(note.Tone, noteDuration, song.BPM)
		if err != nil {
			b.logger.Warn().Err(err).Int("note-num", i).Msg("error playing melody")
			continue
		}

		b.playNote(pitch, length)

	}
	b.logger.Debug().Msg("played song")
}

// notes contains a map of most common note names based on frequency (in hertz)
var notes = map[config.NoteName]NoteFreq{
	"C0":  16.35,
	"C#0": 17.32,
	"Db0": 17.32,
	"D0":  18.35,
	"D#0": 19.45,
	"Eb0": 19.45,
	"E0":  20.60,
	"F0":  21.83,
	"F#0": 23.12,
	"Gb0": 23.12,
	"G0":  24.50,
	"G#0": 25.96,
	"Ab0": 25.96,
	"A0":  27.50,
	"A#0": 29.14,
	"Bb0": 29.14,
	"B0":  30.87,
	"C1":  32.70,
	"C#1": 34.65,
	"Db1": 34.65,
	"D1":  36.71,
	"D#1": 38.89,
	"Eb1": 38.89,
	"E1":  41.20,
	"F1":  43.65,
	"F#1": 46.25,
	"Gb1": 46.25,
	"G1":  49.00,
	"G#1": 51.91,
	"Ab1": 51.91,
	"A1":  55.00,
	"A#1": 58.27,
	"Bb1": 58.27,
	"B1":  61.74,
	"C2":  65.41,
	"C#2": 69.30,
	"Db2": 69.30,
	"D2":  73.42,
	"D#2": 77.78,
	"Eb2": 77.78,
	"E2":  82.41,
	"F2":  87.31,
	"F#2": 92.50,
	"Gb2": 92.50,
	"G2":  98.00,
	"G#2": 103.83,
	"Ab2": 103.83,
	"A2":  110.00,
	"A#2": 116.54,
	"Bb2": 116.54,
	"B2":  123.47,
	"C3":  130.81,
	"C#3": 138.59,
	"Db3": 138.59,
	"D3":  146.83,
	"D#3": 155.56,
	"Eb3": 155.56,
	"E3":  164.81,
	"F3":  174.61,
	"F#3": 185.00,
	"Gb3": 185.00,
	"G3":  196.00,
	"G#3": 207.65,
	"Ab3": 207.65,
	"A3":  220.00,
	"A#3": 233.08,
	"Bb3": 233.08,
	"B3":  246.94,
	"C4":  261.63,
	"C#4": 277.18,
	"Db4": 277.18,
	"D4":  293.66,
	"D#4": 311.13,
	"Eb4": 311.13,
	"E4":  329.63,
	"F4":  349.23,
	"F#4": 369.99,
	"Gb4": 369.99,
	"G4":  392.00,
	"G#4": 415.30,
	"Ab4": 415.30,
	"A4":  440.00,
	"A#4": 466.16,
	"Bb4": 466.16,
	"B4":  493.88,
	"C5":  523.25,
	"C#5": 554.37,
	"Db5": 554.37,
	"D5":  587.33,
	"D#5": 622.25,
	"Eb5": 622.25,
	"E5":  659.25,
	"F5":  698.46,
	"F#5": 739.99,
	"Gb5": 739.99,
	"G5":  783.99,
	"G#5": 830.61,
	"Ab5": 830.61,
	"A5":  880.00,
	"A#5": 932.33,
	"Bb5": 932.33,
	"B5":  987.77,
	"C6":  1046.50,
	"C#6": 1108.73,
	"Db6": 1108.73,
	"D6":  1174.66,
	"D#6": 1244.51,
	"Eb6": 1244.51,
	"E6":  1318.51,
	"F6":  1396.91,
	"F#6": 1479.98,
	"Gb6": 1479.98,
	"G6":  1567.98,
	"G#6": 1661.22,
	"Ab6": 1661.22,
	"A6":  1760.00,
	"A#6": 1864.66,
	"Bb6": 1864.66,
	"B6":  1975.53,
	"C7":  2093.00,
	"C#7": 2217.46,
	"Db7": 2217.46,
	"D7":  2349.32,
	"D#7": 2489.02,
	"Eb7": 2489.02,
	"E7":  2637.02,
	"F7":  2793.83,
	"F#7": 2959.96,
	"Gb7": 2959.96,
	"G7":  3135.96,
	"G#7": 3322.44,
	"Ab7": 3322.44,
	"A7":  3520.00,
	"A#7": 3729.31,
	"Bb7": 3729.31,
	"B7":  3951.07,
	"C8":  4186.01,
	"C#8": 4434.92,
	"Db8": 4434.92,
	"D8":  4698.63,
	"D#8": 4978.03,
	"Eb8": 4978.03,
	"E8":  5274.04,
	"F8":  5587.65,
	"F#8": 5919.91,
	"Gb8": 5919.91,
	"G8":  6271.93,
	"G#8": 6644.88,
	"Ab8": 6644.88,
	"A8":  7040.00,
	"A#8": 7458.62,
	"Bb8": 7458.62,
	"B8":  7902.13,
}
