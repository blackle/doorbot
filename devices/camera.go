package devices

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"github.com/vladimirvivien/go4vl/device"
	"github.com/vladimirvivien/go4vl/v4l2"
	"gitlab.com/queer-computer-club/doorbot/config"
)

const (
	cameraComponentName = "camera"
)

// CameraController defines everything needed to control a USB webcam
type CameraController struct {
	logger  zerolog.Logger
	context context.Context

	device       *device.Device
	CurrentFrame []byte
}

// NewCamera sets up the USB webcam
func NewCameraController(ctx context.Context, l zerolog.Logger, config *config.CameraConfig) (*CameraController, error) {
	// append debug information to the logs
	logger := l.With().Str("component", cameraComponentName).Str("device", config.Device).Logger()

	if !config.Enabled {
		l.Debug().Msg("skipping camera as it is not enabled")
		return &CameraController{logger: l, context: ctx}, nil
	}

	// set up the V4L2 device
	dev, err := device.Open(
		config.Device,
		device.WithPixFormat(v4l2.PixFormat{
			PixelFormat: v4l2.PixelFmtMJPEG,
			Width:       640,
			Height:      480,
		}),
		device.WithBufferSize(0),
		device.WithVideoCaptureEnabled(),
	)
	if err != nil {
		return nil, err
	}

	// start the camera
	if err := dev.Start(ctx); err != nil {
		return nil, err
	}

	cam := &CameraController{
		logger:  logger,
		context: ctx,
		device:  dev,
	}

	logger.Info().Msg("initialized camera controller")

	return cam, nil
}

// Start begins the loop that accepts commands for the camera
func (cam *CameraController) Start(waitGroup *sync.WaitGroup) {
	if cam.device == nil {
		return
	}
	cam.logger.Debug().Msg("starting camera controller")
	go cam.cameraLoop(waitGroup)
	cam.logger.Info().Msg("started camera controller")
}

// main camera loop (currently unused, but could be used for something else later)
func (cam *CameraController) cameraLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	cam.logger.Debug().Msg("beginning camera loop")

	for {
		select {
		case <-cam.context.Done():
			cam.logger.Debug().Msg("stopping camera controller")
			cam.device.Close()
			cam.logger.Info().Msg("stopped camera controller")
			waitGroup.Done()
			return

		case <-time.After(1 * time.Millisecond):
			if cam.device != nil {
				frame := <-cam.device.GetOutput()
				if len(frame) > 0 {
					cam.CurrentFrame = frame
				} else {
					cam.CurrentFrame = make([]byte, 0)
				}
			}
			continue
		}
	}
}
