package devices

import (
	"context"
	"fmt"
	"math"
	"sync"
	"time"

	ws2811 "github.com/rpi-ws281x/rpi-ws281x-go"
	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
)

type LEDEffect int

const (
	ledComponentName      = "led"
	ledQueueComponentName = "led-queue"

	// LEDEffects are basic effects used for the LED ring, i.e: when a card is accepted/rejected, or when the reader is waiting
	LEDAccept LEDEffect = iota
	LEDDeny
	LEDOpen
	LEDOff
	LEDDoorbell
	LEDWaiting
	LEDCancel

	// Colours are specified in hex
	ColourRed   = 0xff0000
	ColourGreen = 0x00ff00
	ColourBlue  = 0x0000ff
	ColourWhite = 0xffffff
	ColourBlack = 0x000000

	defaultRainbowSpeed = 200
)

// RingLEDController controls the LED ring in the doorbot scanner module
// ws2811 is the type of LED controller embedded into the LEDs themselves
type RingLEDController struct {
	logger              zerolog.Logger
	context             context.Context
	effectSwitchContext context.Context
	effectSwitchCancel  context.CancelFunc

	device            *ws2811.WS2811
	ledCount          int
	currentBrightness int

	playLEDQueue *ledQueue
}

// NewRingLEDController initializes a new RingLEDController object for controlling different LED strips
func NewRingLEDController(ctx context.Context, l zerolog.Logger, cfg *config.LEDConfig) (*RingLEDController, error) {
	logger := l.With().Str("component", ledComponentName).Logger()

	// default options for LED ring
	opt := ws2811.DefaultOptions
	opt.Channels[0].LedCount = cfg.Count
	opt.Channels[0].GpioPin = cfg.GPIOPin

	dev, err := ws2811.MakeWS2811(&opt)
	if err != nil {
		return nil, fmt.Errorf("could not create ring: %v", err)
	}

	// initialize LED strip
	err = dev.Init()
	if err != nil {
		return nil, fmt.Errorf("could not initialize ring: %v", err)
	}

	// effectSwitchContext is used to signal that the waiting program should end
	// so that more important LED effects should play
	// (such as blinking green when someone swipes a card)
	effectSwitchContext, effectSwitchCancel := context.WithCancel(context.Background())

	led := &RingLEDController{
		logger:              logger,
		context:             ctx,
		effectSwitchContext: effectSwitchContext,
		effectSwitchCancel:  effectSwitchCancel,
		device:              dev,
		ledCount:            cfg.Count,
		currentBrightness:   cfg.Brightness,
		playLEDQueue:        newLEDQueue(l, 12),
	}

	// ensure all colours are working
	led.logger.Debug().Msg("running LED test")
	led.setBrightness(0, 96)
	led.wipe(ColourRed, 20)
	led.wipe(ColourGreen, 20)
	led.wipe(ColourBlue, 20)
	led.wipe(ColourBlack, 20)
	led.effectSwitchCancel()

	logger.Info().Msg("initialized LED device")

	return led, nil
}

// Start begins the loop that accepts commands for the LED ring
func (led *RingLEDController) Start(waitGroup *sync.WaitGroup) {
	led.logger.Debug().Msg("starting LED ring controller")
	go led.ledLoop(waitGroup)
	led.logger.Info().Msg("started LED ring controller")
}

// ledLoop accepts commands through the ledQueue, playing the program on the RGB LED ring
// it will also interrupt LEDWaiting programs to interject more important onces (such as the LEDAccept)
func (led *RingLEDController) ledLoop(waitGroup *sync.WaitGroup) {
	led.logger.Debug().Msg("beginning LED controller loop")
	waitGroup.Add(1)

	for {
		// set a default duration
		duration := time.Duration(10 * time.Millisecond)

		// ensure that something exists in the queue
		if len(led.playLEDQueue.programs) > 0 {
			program := led.playLEDQueue.pop()
			if program != nil {
				led.effectSwitchCancel() // cancel LEDWaiting
				go led.playEffect(program.effect, program.brightness)
				duration = time.Duration(program.duration * time.Second)
				led.logger.Debug().Int("effect", int(program.effect)).Dur("duration", duration).Msg("got LED program")
			}
		}

		select {
		case <-led.context.Done():
			led.logger.Debug().Msg("stopping LED controller")
			led.effectSwitchCancel()
			led.wipe(ColourBlack, 0)
			led.device.Fini()
			led.logger.Info().Msg("stopped LED controller")
			waitGroup.Done()
			return

		case <-time.After(duration):
			continue
		}
	}
}

// SetLEDEffect sets the effect to be played on the doorbot LED ring
func (led *RingLEDController) SetLEDEffect(effect LEDEffect, brightness int, duration time.Duration) {
	led.logger.Trace().Int("effect", int(effect)).Int("brightness", brightness).Dur("duration", duration).Msg("adding to LED queue")

	effectProgram := ledProgram{
		effect:     effect,
		brightness: brightness,
		duration:   duration,
	}

	led.playLEDQueue.push(effectProgram)

	time.Sleep(10 * time.Millisecond)

	// reset back LEDWaiting
	if effect != LEDWaiting && effect != LEDCancel && effect != LEDOff {
		waitingEffect := ledProgram{
			effect:     LEDWaiting,
			brightness: brightness,
			duration:   2,
		}
		led.playLEDQueue.push(waitingEffect)
	}
}

// fadeBrightness sets the brightness to fade over a period of time, in milliseconds
func (led *RingLEDController) fadeBrightness(to int, delayMilli int) {
	led.logger.Trace().Int("current-brightness", led.currentBrightness).Int("to-brightness", to).Msg("fadeBrightness")

	if led.currentBrightness > to {
		for brightness := led.currentBrightness; brightness > to; brightness-- {
			led.device.SetBrightness(0, brightness)
			led.device.Render()
			time.Sleep(time.Duration(delayMilli) * time.Millisecond)
		}
	} else {
		for brightness := led.currentBrightness; brightness < to; brightness++ {
			led.device.SetBrightness(0, brightness)
			led.device.Render()
			time.Sleep(time.Duration(delayMilli) * time.Millisecond)
		}
	}
	led.currentBrightness = to
}

func (led *RingLEDController) setBrightness(strip, value int) {
	led.device.SetBrightness(strip, value)
	led.currentBrightness = value
}

// playEffect defines the effects that can be played on LED ring
func (led *RingLEDController) playEffect(effect LEDEffect, brightness int) {
	switch effect {
	case LEDAccept:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDAccept initiated")
		led.setBrightness(0, brightness)
		led.wipe(ColourBlack, 0)
		led.flash(ColourGreen, 200, 200)
		led.flash(ColourGreen, 200, 200)
		led.flash(ColourGreen, 600, 0)

	case LEDDoorbell:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDDoorbell initiated")
		led.effectSwitchCancel()
		led.wipe(ColourBlack, 0)
		led.flash(ColourBlue, 200, 200)
		led.flash(ColourBlue, 600, 200)
		led.flash(ColourBlue, 200, 200)
		led.flash(ColourBlue, 600, 200)

	case LEDDeny:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDDeny initiated")
		led.setBrightness(0, brightness)
		led.wipe(ColourBlack, 0)
		led.flash(ColourRed, 200, 200)
		led.flash(ColourRed, 200, 200)
		led.flash(ColourRed, 600, 0)

	case LEDOpen:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDOpen initiated")
		led.effectSwitchCancel()
		go led.fadeBrightness(brightness, 1)
		led.wipe(ColourBlack, 0)
		led.wipe(ColourGreen, 0)

	case LEDWaiting:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDWaiting initiated")
		led.setBrightness(0, 0)
		go led.fadeBrightness(brightness, 10)
		go led.rainbow(defaultRainbowSpeed)

	case LEDOff:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDOff initiated")
		led.effectSwitchCancel()
		led.wipe(ColourBlack, 0)

	case LEDCancel:
		led.logger.Trace().Int("brightness", brightness).Msg("LEDCancel initiated")
		led.effectSwitchCancel()
		led.fadeBrightness(0, 10)

	default:
		led.logger.Error().Int("effect", int(effect)).Msg("LED program not found")
	}
}

// ledQueue is a queue for LED programs, that can then be run in sequence
type ledQueue struct {
	logger        zerolog.Logger
	currentEffect LEDEffect
	programs      chan ledProgram
}

// ledProgram is for controlling which effect displays on the ring, and for resetting when certain events occur
type ledProgram struct {
	effect     LEDEffect
	duration   time.Duration // in seconds
	brightness int           // 0-255 (but try to keep it around 128)
}

// newLEDQueue creates a new queue for LED effects
func newLEDQueue(l zerolog.Logger, size int) *ledQueue {
	logger := l.With().Str("component", ledQueueComponentName).Logger()
	return &ledQueue{
		logger:        logger,
		currentEffect: LEDOff,
		programs:      make(chan ledProgram, size),
	}
}

// pop removes one LED effect from the queue
func (ledQueue *ledQueue) pop() *ledProgram {
	select {
	case program := <-ledQueue.programs:
		return &program
	default:
		ledQueue.logger.Error().Msg("LED program queue empty")
		return nil
	}
}

// Push adds one LED effect to the queue
func (ledQueue *ledQueue) push(program ledProgram) {
	select {
	case ledQueue.programs <- program:
	default:
		ledQueue.logger.Error().Msg("LED program queue full")
	}
}

// flash is a helper command which flashes a single colour
// durationOn is how long the colour should be visible (in milliseconds)
// durationOff sets how long the "off" period should be immediately afterwards
func (led *RingLEDController) flash(color uint32, durationOn time.Duration, durationOff time.Duration) {
	led.wipe(color, 0)
	time.Sleep(durationOn * time.Millisecond)
	led.wipe(ColourBlack, 0)
	time.Sleep(durationOff * time.Millisecond)
}

// wipe sets the entire LED ring to a single colour
// it can be done instantaneously or over the course of a settable delay per-pixel (in milliseconds)
func (led *RingLEDController) wipe(color uint32, delayMilli time.Duration) error {
	for i := 0; i < len(led.device.Leds(0)); i++ {
		led.device.Leds(0)[i] = color
		if err := led.device.Render(); err != nil {
			return err
		}
		if delayMilli > 0 {
			time.Sleep(delayMilli * time.Millisecond)
		}
	}
	return nil
}

// rainbow creates a seamless rainbow effect that spins around the LEDs
func (led *RingLEDController) rainbow(delayMilli time.Duration) {
	// start new context
	led.effectSwitchContext, led.effectSwitchCancel = context.WithCancel(context.Background())

	for { // run forever
		for r := 0; r < 256; r++ { // the number of colours we're working with - 256

			for pixel := 0; pixel < led.ledCount; pixel++ { // the number of pixels in the strip/ring
				index := math.Floor(float64(pixel*256/led.ledCount)) + float64(r)
				r, g, b := colourWheel(uint32(index) & 255)
				colour := rgbToColour(r, g, b)
				led.device.Leds(0)[pixel] = colour
			}

			led.device.Render()
			select {
			case <-led.effectSwitchContext.Done():
				// exit from this early
				return
			case <-time.After(delayMilli * time.Millisecond):
				continue
			}
		}
	}
}

// colourWheel takes in a pixel position and provides an RGB value
func colourWheel(p uint32) (r, g, b uint32) {
	p = 255 - p
	if p < 85 {
		return 255 - p*3, 0, p * 3
	}
	if p < 170 {
		p -= 85
		return 0, p * 3, 255 - p*3
	}

	p -= 170

	return p * 3, 255 - p*3, 0
}

// rgbToColour takes in RGB values and produces a uint32 colour value
func rgbToColour(r uint32, g uint32, b uint32) uint32 {
	return (r<<16 | g<<8 | b)
}
