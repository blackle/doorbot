package main

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
	"gitlab.com/queer-computer-club/doorbot/devices"
	"gitlab.com/queer-computer-club/doorbot/services"
)

const (
	doorbotComponentName   = "doorbot-main"
	defaultDoorbellMessage = "Ding dong! The doorbell was rung!"
	defaultDoorOpenWarning = "Uh oh! The door seems to have been opened for longer than %d minute(s). Please close the door!"
	cameraNotFoundMessage  = "Warning! Camera could not be initialized!"
)

// Doorbot struct contains everything related to managing doorbot
type Doorbot struct {
	logger     *zerolog.Logger
	config     *config.DoorbotConfig
	runContext context.Context
	runCancel  context.CancelFunc
	waitGroup  *sync.WaitGroup

	// devices
	membersDB  *config.MembersDB
	doorstrike *devices.DoorStrike
	reader     *devices.Reader
	buzzer     *devices.Buzzer
	leds       *devices.RingLEDController
	misc       *devices.MiscController
	camera     *devices.CameraController

	// services
	services *services.ServiceManager
}

// NewDoorbot initializes all major components of doorbot, such as devices and internet services
func NewDoorbot(l *zerolog.Logger, cfg *config.DoorbotConfig, secrets *config.SecretsConfig, fakeNFCFlag bool) (*Doorbot, error) {
	logger := l.With().Str("component", doorbotComponentName).Logger()

	logger.Trace().Msg("NewDoorbot initiated")

	runContext, runCancel := context.WithCancel(context.Background())

	// initialize the members database
	database, err := config.GetMembersDatabase(*l, cfg)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize members DB")
	}

	reader, err := devices.NewCardReader(runContext, *l, fakeNFCFlag)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize card reader")
	}

	doorstrike, err := devices.NewDoorStrike(runContext, *l, &cfg.Doorstrike)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize door strike")
	}

	readerLEDConfig := cfg.LEDs[0]
	leds, err := devices.NewRingLEDController(runContext, *l, &readerLEDConfig)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize LED ring")
	}

	buzzer, err := devices.NewBuzzerController(runContext, *l, cfg)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize buzzer")
	}

	misc, err := devices.NewMiscController(runContext, *l, &cfg.Misc)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize misc devices")
	}

	camera, err := devices.NewCameraController(runContext, *l, &cfg.Camera)
	if err != nil {
		logger.Error().Err(err).Msg("failed to initialize camera")
	}

	var frameToServices *[]byte
	if camera != nil {
		frameToServices = &camera.CurrentFrame
	}

	services, err := services.NewServiceManager(runContext, *l, cfg, secrets, database, frameToServices)
	if err != nil {
		logger.Error().Err(err).Msg("failed to initialize service manager")
	}

	// check if the camera was supposed to be enabled, and if it failed, post a message
	if camera == nil && cfg.Camera.Enabled {
		services.PostBasic(cameraNotFoundMessage)
	}

	var waitGroup sync.WaitGroup

	return &Doorbot{
		logger:     &logger,
		config:     cfg,
		runContext: runContext,
		runCancel:  runCancel,
		waitGroup:  &waitGroup,
		membersDB:  database,
		doorstrike: doorstrike,
		reader:     reader,
		buzzer:     buzzer,
		leds:       leds,
		misc:       misc,
		camera:     camera,
		services:   services,
	}, nil
}

// doorbotLoop performs various checks needed to operate doorbot
func (bot *Doorbot) doorbotLoop() {
	bot.logger.Info().Msg("beginning door checker")
	for {
		select {
		case eventMode := <-bot.misc.EventModeChan:
			if eventMode {
				// prevent duplicate logs
				if bot.misc.EventModeEnabled {
					continue
				}
				bot.logger.Debug().Msg("event mode enabled")
				bot.misc.EventModeEnabled = true
				bot.doorstrike.UnlockDoor()
				bot.leds.SetLEDEffect(devices.LEDOpen, 96, 0)
				bot.services.PostBasic("Event mode enabled!")
			} else {
				// prevent duplicate logs
				if !bot.misc.EventModeEnabled {
					continue
				}
				bot.logger.Debug().Msg("event mode disabled")
				bot.misc.EventModeEnabled = false
				bot.doorstrike.LockDoor()
				bot.leds.SetLEDEffect(devices.LEDOff, 96, 0)
				bot.services.PostBasic("Event mode disabled!")
			}

		case card := <-bot.reader.DetectedID:
			// clear the card but skip everything else
			if bot.misc.EventModeEnabled {
				bot.logger.Info().Msg("skipping card processing of ID card as event mode is enabled")
				time.Sleep(100 * time.Millisecond) // sleep so the thread doesn't spin
				continue
			}
			// WARNING: this block controls unlocking the door
			// do not edit unless you want to commit to thorough testing
			member, err := bot.membersDB.CompareCardToMembers(bot.logger, card)
			if err != nil {
				bot.logger.Error().Err(err).Msg("unauthorized")
				bot.leds.SetLEDEffect(devices.LEDDeny, 128, 4)
				bot.buzzer.PlayChime(devices.DefaultDenyChime)
				continue
			} else {
				logger := bot.logger.With().Str("member-id", member.Username).Logger()
				logger.Debug().Msg("member card detected, opening door")

				// unlock the door
				err = bot.doorstrike.UnlockForDuration(time.Duration(bot.config.Doorstrike.OpenTime) * time.Millisecond)
				if err != nil {
					logger.Error().Err(err).Msg("could not unlock door")
				}
				bot.leds.SetLEDEffect(devices.LEDAccept, 96, 4)

				logger.Info().Msg("opened door for member")
				// process the rest of the entry now that the member has access

				// if the member doesn't have any preferences for when they enter, play the default chime and stop there
				if !member.EntryPreference.Enabled {
					// play default chime and leave it at that
					bot.buzzer.PlayChime(devices.DefaultAcceptChime)
					continue
				}

				bot.buzzer.PlayChime(devices.ChimeID(member.EntryPreference.PreferredSongID))
				bot.services.PostMemberEntryMessage(member.ID)

				logger.Info().Msg("processed entry preferences")
			}

		case <-bot.services.DoorOpenChan:
			// clear the card but skip everything else
			if bot.misc.EventModeEnabled {
				bot.logger.Info().Msg("skipping discord unlock as event mode is enabled")
				time.Sleep(100 * time.Millisecond)
				continue
			}

			bot.logger.Debug().Msg("unlocking door through messaging services")

			// unlock the door
			err := bot.doorstrike.UnlockForDuration(time.Duration(bot.config.Doorstrike.OpenTime) * time.Millisecond)
			if err != nil {
				bot.logger.Error().Err(err).Msg("could not unlock door")
				bot.leds.SetLEDEffect(devices.LEDDeny, 96, 4)
				continue
			}

			bot.logger.Debug().Int("default-accept-chime", int(devices.DefaultAcceptChime)).Msg("playing chime")
			bot.leds.SetLEDEffect(devices.LEDAccept, 96, 4)
			bot.buzzer.PlayChime(devices.DefaultAcceptChime)
			bot.logger.Info().Msg("unlocked door through messager action")

		case <-bot.services.ReloadMembersChan:
			bot.logger.Debug().Msg("reloading member's list via messager action")
			membersDB, err := config.GetMembersDatabase(*bot.logger, bot.config)
			if err != nil {
				bot.logger.Error().Err(err).Msg("could not reload member's list; reverting to old list")
				continue
			}
			bot.membersDB = membersDB
			bot.logger.Info().Msg("reloaded member's list via messager action")

		case <-bot.misc.DoorOpenNotificationChan:
			bot.services.PostBasic(fmt.Sprintf(defaultDoorOpenWarning, bot.config.Misc.DoorOpenTime))
			bot.logger.Info().Msg("detected door opened longer than expected; sent messager notification")

		case <-bot.misc.DoorbellChan:
			bot.buzzer.PlayChime(devices.DefaultDoorBell)
			bot.services.PostWithImage(defaultDoorbellMessage, bot.camera.CurrentFrame)
			bot.leds.SetLEDEffect(devices.LEDDoorbell, 96, 6)
			bot.logger.Info().Msg("doorbell rung")

		case motionEvent := <-bot.misc.MotionChan:
			if bot.misc.EventModeEnabled {
				bot.logger.Debug().Msg("motion event as Event Mode is enabled")
				time.Sleep(100 * time.Millisecond)
				continue
			}
			bot.logger.Trace().Bool("motion-event", motionEvent).Msg("motion event detected")
			if motionEvent {
				// skip if Event Mode is enabled
				if bot.misc.EventModeEnabled {
					bot.logger.Trace().Msg("motion event discarded; event mode active")
					bot.leds.SetLEDEffect(devices.LEDOpen, 96, 10)
					time.Sleep(100 * time.Millisecond) // sleep so the thread doesn't spin
					continue
				}

				if bot.misc.MotionNewEvent {
					bot.misc.MotionNewEvent = false
					bot.leds.SetLEDEffect(devices.LEDWaiting, 96, 1)
				}

				continue
			}

			bot.misc.MotionNewEvent = true
			bot.leds.SetLEDEffect(devices.LEDCancel, 96, 2)

		case <-bot.runContext.Done():
			bot.logger.Info().Msg("stopping main doorbot controller")
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}

// Start launches all devices and services needed to run doorbot
func (bot *Doorbot) Start() {
	bot.doorstrike.Start(bot.waitGroup)
	bot.reader.Start(bot.waitGroup)
	bot.buzzer.Start(bot.waitGroup)
	bot.leds.Start(bot.waitGroup)
	bot.misc.Start(bot.waitGroup)
	bot.camera.Start(bot.waitGroup)
	bot.services.Start(bot.waitGroup)
	go bot.doorbotLoop()
}

// Stop shuts down all processes during exit
func (bot *Doorbot) Stop() {
	bot.runCancel() // sends a signal through the Context to end all sub-processes (devices, services, etc)
	bot.logger.Info().Msg("waiting for all devices/services to exit")
	bot.waitGroup.Wait() // this ensures that everything ends before the program terminates
	bot.logger.Info().Msg("all devices/services have exited")
}
