module gitlab.com/queer-computer-club/doorbot

go 1.21

require (
	github.com/StarWitch/nfc/v2 v2.1.4
	github.com/bwmarrin/discordgo v0.27.1
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.10
	github.com/rs/zerolog v1.31.0
	github.com/shirou/gopsutil v3.21.11+incompatible
	github.com/stianeikeland/go-rpio/v4 v4.6.0
	github.com/stretchr/testify v1.8.4
	github.com/vladimirvivien/go4vl v0.0.5
	golang.org/x/text v0.14.0
	periph.io/x/conn/v3 v3.7.0
	periph.io/x/host/v3 v3.8.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/tklauser/go-sysconf v0.3.13 // indirect
	github.com/tklauser/numcpus v0.7.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.3 // indirect
	golang.org/x/crypto v0.16.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
