package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

// Variables used for command line parameters
var (
	Token string
)

func init() {

	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// In this example, we only care about receiving message events.
	dg.Identify.Intents = discordgo.IntentsGuildMessages

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	fmt.Println("hello")
	cat, err := os.Open("cat.jpg")
	if err != nil {
		fmt.Println("failed")
	}
	//If the message is "ping" reply with a basic embed.
	if m.Content == "ping" {
		embed := &discordgo.MessageEmbed{
			Title: "Doorbot Bell Notification",
			//URL:         "https://github.com/bwmarrin/discordgo",
			Description: "Ding dong! Doorbell has been rung!",
			Color:       0xc378de,
			Image:       &discordgo.MessageEmbedImage{URL: "attachment://image.jpg"},
			Fields: []*discordgo.MessageEmbedField{
				{
					Name:   "If you recognize this person, use the following command:",
					Value:  "/open-door",
					Inline: true,
				},
			},
		}
		post := &discordgo.MessageSend{
			Embeds: []*discordgo.MessageEmbed{embed},
			Files:  []*discordgo.File{{Name: "image.jpg", ContentType: "image/jpeg", Reader: cat}},
		}
		_, err := s.ChannelMessageSendComplex(m.ChannelID, post)
		if err != nil {
			fmt.Println("error received: %w", err)
		}
	}
}
