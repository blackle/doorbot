package main

// this is intended as a "template" file for adding devices or services with contexts and loops

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
)

// DeviceController does such and such
type DeviceController struct {
	logger  zerolog.Logger
	context context.Context
}

// NewDeviceController creates a new device
func NewDeviceController(ctx context.Context, l zerolog.Logger, config *config.DoorbotConfig) (*DeviceController, error) {
	// create decorated logger object
	logger := l.With().Str("component", "device").Logger()

	controller := &DeviceController{
		logger:  logger,
		context: ctx,
	}

	return controller, nil
}

// Start begins the loop that accepts commands for the device
func (dev *DeviceController) Start(wg *sync.WaitGroup) {
	dev.logger.Debug().Msg("starting device controller")
	go dev.deviceLoop(wg)
	dev.logger.Info().Msg("started device controller")
}

// main device loop
func (dev *DeviceController) deviceLoop(wg *sync.WaitGroup) {
	dev.logger.Debug().Msg("beginning device loop")
	wg.Add(1)

	for {
		select {
		case <-dev.context.Done():
			dev.logger.Info().Msg("stopping device controller")
			wg.Done()
			return
		case <-time.After(100 * time.Millisecond):
			continue
		}
	}
}
