# TODO List

## Devices

### Sign
* A new module that can send commands to an LED display inside the space (over the network?)

## Services

### Interface
* Make an interface that is more (small-g) generic so that Doorbot can post to more than just Discord (Matrix, IRC, whatever)

### Discord
* `/help` command for displaying common doorbot commands to those who ask
* `/reload` for reloading the member list w/o having to bring doorbot down
* `/logs` command for Discord which posts the log file (only accessible to the Director role)
* Cut down on entry notification noise by appending multiplier emojis to entry logs done within 3-4 hours (use redis?)
* Convert all usernames to absolute user IDs for Discord notifications
* Automatically assign roles based on membership

## Bugs
* `LEDOpen` doesn't work for Event Mode
* Webcam disappearing leads to service restart loop (make it re-initalize USB instead?)

### Web Management
* A web interface to modify settings
    - Management of doorbot settings
    - Management of members
    - Management of entry preferences (for members)
