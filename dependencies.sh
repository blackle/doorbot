#!/bin/bash

apt update
apt install i2c-tools libi2c-dev gawk python3-dev libtool autoconf meson cmake

rm -rf /usr/local/go
curl -LO https://go.dev/dl/go1.21.5.linux-arm64.tar.gz
tar -xpf ./go1.21.5.linux-arm64.tar.gz -C /usr/local
rm ./go1.21.5.linux-arm64.tar.gz
rm -f /usr/local/bin/go && ln -s /usr/local/go/bin/go /usr/local/bin/go

