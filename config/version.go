package config

import "flag"

var DoorbotVersion string

const (
	DoorbotConfDefaultPath = "/etc/doorbot/settings.conf"
)

// GetVersion sets the version of doorbot
// this is set during compilation
// see the Makefile for implementation
func GetVersion() string {
	return DoorbotVersion
}

func GetCommandFlags() (*bool, *bool, *bool, *string) {
	// put your configuration flags here
	debugFlag := flag.Bool("debug", false, "enable debug logging")
	traceFlag := flag.Bool("trace", false, "enable trace logging")
	fakeNFCFlag := flag.Bool("fake-nfc", false, "enable fake NFC mode (disables reader but allows boot without it being connected")
	configFlag := flag.String("config", DoorbotConfDefaultPath, "absolute path to the config file")
	flag.Parse()
	return debugFlag, traceFlag, fakeNFCFlag, configFlag
}
