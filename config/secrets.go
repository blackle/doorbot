package config

import (
	"encoding/json"
	"os"
)

// SecretsConfig specifies the config file structure in JSON for Doorbot
type SecretsConfig struct {
	Discord DiscordBot `json:"discord"`
}

type DiscordBot struct {
	Enabled        bool     `json:"enabled"`
	BotToken       string   `json:"botToken"`
	BotName        string   `json:"botName"`
	ChannelID      string   `json:"channelID"`
	GuildID        string   `json:"guildID"`
	MemberRoleIDs  []string `json:"memberRoleIDs"`
	DirectorRoleID string   `json:"directorRoleID"`
}

// Get ensures that config files exist and are readable
func GetSecrets(configPath string) (*SecretsConfig, error) {
	config := &SecretsConfig{}

	configFile, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer configFile.Close()

	decoder := json.NewDecoder(configFile)
	decoder.DisallowUnknownFields() // ensure no junk data is in the config file
	if err := decoder.Decode(config); err != nil {
		return nil, err
	}

	return config, nil
}
