package config

import (
	"encoding/json"
	"os"
)

// DoorbotConfig specifies the config file structure in JSON for Doorbot
type DoorbotConfig struct {
	Doorstrike DoorstrikeConfig  `json:"doorstrikeConfig"`
	LEDs       map[int]LEDConfig `json:"ledConfig"`
	Buzzer     BuzzerConfig      `json:"buzzerConfig"`
	Misc       MiscConfig        `json:"miscConfig"`
	Camera     CameraConfig      `json:"cameraConfig"`
	Paths      Paths             `json:"paths"`
	Logging    Logging           `json:"logging"`
}

// Logging defines variables for the logging service
type Logging struct {
	RemoteAddr       string `json:"remoteAddr"`
	RemoteEnabled    bool   `json:"remoteEnabled"`
	LocalFilePath    string `json:"localFilePath"`
	LocalFileEnabled bool   `json:"localFileEnabled"`
	Debug            bool   `json:"debugEnabled"`
	Trace            bool   `json:"traceEnabled"`
}

// Paths specifies the paths for each config file
type Paths struct {
	SongsDBFile  string `json:"songsDBFile"`
	MemberDBFile string `json:"memberDBFile"`
	SecretsFile  string `json:"secretsFile"`
}

// BuzzerConfig controls the speaker for chimes and doorbells
type BuzzerConfig struct {
	Enabled bool `json:"enabled"`
	Pin     int  `json:"pin"`
}

// DoorstrikeConfig specifies a sub-heading in JSON for Doorstrike-specific functions
type DoorstrikeConfig struct {
	StrikePin string `json:"strikePin"` // controls the actual doorstrike
	OpenTime  int    `json:"openTime"`  // in milliseconds (i.e: 3000 = 3 seconds)
}

// LEDConfig controls the door reader's LED status ring
type LEDConfig struct {
	Brightness int `json:"brightness"` // brightness level (0-255)
	Count      int `json:"count"`      // how many LEDs
	GPIOPin    int `json:"gpioPin"`    // which pin it's connected to
}

// CameraConfig is for controlling the USB webcam
type CameraConfig struct {
	Enabled bool   `json:"enabled"`
	Device  string `json:"device"` // what device the webcam is hooked up to
}

// MiscConfig is for other small devices hooked up to doorbot
type MiscConfig struct {
	MotionTime      int    `json:"motionTime"`      // how long LEDs should stay active after detecting motion (in seconds)
	DoorOpenTime    int    `json:"doorOpenTime"`    // how long the door can remain open before sending a notification (in minutes)
	DoorOpenEnabled bool   `json:"doorOpenEnabled"` // whether the door open alarm should be triggered at all
	MotionPin       string `json:"motionPin"`       // which pin the motion detector is connected to (i.e: "GPIOXX")
	DoorbellPin     string `json:"doorbellPin"`     // which pin the doorbell is connected to (i.e: "GPIOXX")
	DoorOpenPin     string `json:"doorOpenPin"`     // which pin the door open detector is connected to (i.e: "GPIOXX")
	EventModePin    string `json:"eventModePin"`    // controls the event mode switch to allow the strike to stay open
}

// Get ensures that config files exist and are readable
func Get(configPath string) (*DoorbotConfig, error) {
	config := &DoorbotConfig{}

	configFile, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer configFile.Close()

	decoder := json.NewDecoder(configFile)
	decoder.DisallowUnknownFields() // ensure no junk data is in the config file
	if err := decoder.Decode(config); err != nil {
		return nil, err
	}

	return config, nil
}
