package config

import (
	"encoding/json"
	"os"
)

type NoteName string

const (
	// these are the shorthand note names used in the SongsDB config
	WholeNoteName     = "Whole"
	HalfNoteName      = "Half"
	QuarterNoteName   = "Quarter"
	EighthNoteName    = "Eighth"
	SixteenthNoteName = "Sixteenth"
)

// SongsDB defines the structure of the JSON file that houses songs
type SongsDB struct {
	Songs map[int]Song `json:"songs"`
}

// Song defines everything needed to play a song, including its name, BPM, and the melody
type Song struct {
	Name   string       `json:"name"`
	Melody map[int]Note `json:"melody"` // "1" : { "note": "C4", "duration": "Quarter" }
	BPM    int          `json:"bpm"`
}

// Note defines a single note in a melody
type Note struct {
	Tone     NoteName `json:"note"`
	Duration string   `json:"duration"`
}

// GetSongDB returns the complete library of chimes/songs available for doorbot
func GetSongsDB(path string) (*SongsDB, error) {
	songsConfig := &SongsDB{}

	songsConfigFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer songsConfigFile.Close()

	decoder := json.NewDecoder(songsConfigFile)
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(songsConfig); err != nil {
		return nil, err
	}

	return songsConfig, nil
}
