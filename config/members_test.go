package config_test

import (
	"fmt"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"gitlab.com/queer-computer-club/doorbot/config"
)

// TestCompareCardToMembers tests to ensure that the comparison of member IDs actually occurs and doesn't break somehow
func TestCompareCardToMembers(t *testing.T) {
	logger := zerolog.Nop()

	tests := map[string]struct {
		members        []config.Member
		cardScanned    config.PhysicalCard
		expectedMember *config.Member
		expectedResult error
	}{
		"test active, one member": {
			members: []config.Member{
				{
					ID:       1,
					Name:     "quecey",
					Username: "qc",
					Active:   true,
					ContactPreference: config.ContactPreference{
						Email: "quecey@example.com",
					},
					PhysicalCard: []config.PhysicalCard{
						{
							ID: "123456",
						},
					},
				},
			},
			cardScanned: config.PhysicalCard{
				ID: "123456",
			},
			expectedMember: &config.Member{
				Name:     "quecey",
				Username: "qc",
				PhysicalCard: []config.PhysicalCard{
					{
						ID: "123456",
					},
				},
			},
			expectedResult: nil,
		},
		"test active, multiple members": {
			members: []config.Member{
				{
					ID:       1,
					Name:     "quecey",
					Username: "qc",
					Active:   true,
					ContactPreference: config.ContactPreference{
						Email: "quecey@example.com",
					},
					PhysicalCard: []config.PhysicalCard{
						{
							ID: "12345678",
						},
					},
				},
				{
					ID:       2,
					Name:     "quecey2",
					Username: "qc2",
					Active:   true,
					ContactPreference: config.ContactPreference{
						Email: "quecey2@example.com",
					},
					PhysicalCard: []config.PhysicalCard{
						{
							ID: "123456",
						},
					},
				},
				{
					ID:       3,
					Name:     "quecey3",
					Username: "qc3",
					Active:   true,
					ContactPreference: config.ContactPreference{
						Email: "quecey3@example.com",
					},
					PhysicalCard: []config.PhysicalCard{
						{
							ID: "123456789",
						},
					},
				},
			},
			cardScanned: config.PhysicalCard{
				ID: "123456",
			},
			expectedMember: &config.Member{
				Name:     "quecey2",
				Username: "qc2",
				PhysicalCard: []config.PhysicalCard{
					{
						ID: "123456",
					},
				},
			},
			expectedResult: nil,
		},
		"test inactive": {
			members: []config.Member{
				{
					ID:       1,
					Name:     "quecey",
					Username: "qc",
					Active:   false,
					ContactPreference: config.ContactPreference{
						Email: "quecey@example.com",
					},
					PhysicalCard: []config.PhysicalCard{
						{
							ID: "123456",
						},
					},
				},
			},
			cardScanned: config.PhysicalCard{
				ID: "123456",
			},
			expectedMember: nil,
			expectedResult: fmt.Errorf("member not active [ID: 1, Username: qc]"),
		},
		"test wrong ID": {
			members: []config.Member{
				{
					ID:       1,
					Name:     "quecey",
					Username: "qc",
					ContactPreference: config.ContactPreference{
						Email: "quecey@example.com",
					},
					Active: true,
					PhysicalCard: []config.PhysicalCard{
						{
							ID: "1234567",
						},
					},
				},
			},
			cardScanned: config.PhysicalCard{
				ID: "123456",
			},
			expectedMember: nil,
			expectedResult: fmt.Errorf("member not found with card ID: 123456"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			membersDB := &config.MembersDB{Members: test.members}
			member, err := membersDB.CompareCardToMembers(&logger, test.cardScanned)
			if err != nil {
				if !strings.Contains(test.expectedResult.Error(), err.Error()) {
					t.Fatal(err)
				}
			} else {
				require.Equal(t, member.Name, test.expectedMember.Name)
				require.Equal(t, member.Username, test.expectedMember.Username)
				require.Equal(t, member.PhysicalCard[0].ID, test.cardScanned.ID)
			}
		})
	}
}
