package config

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
)

type PrimaryContact int

const (
	PrefersEmail PrimaryContact = iota
	PrefersDiscord
)

// Members contains a list of Member objects
type MembersDB struct {
	Members []Member `json:"members"`
}

// Member defines the JSON structure of members
type Member struct {
	ID                int               `json:"id"`       // unique ID
	Name              string            `json:"name"`     // preferred name
	Username          string            `json:"username"` // preferred username
	Active            bool              `json:"active"`   // whether the member has an active membership
	Pronouns          []string          `json:"pronouns"`
	EntryPreference   EntryPreference   `json:"entryPreference"`
	ContactPreference ContactPreference `json:"contactPreference"`
	PhysicalCard      []PhysicalCard    `json:"physicalCards"` // yes, a list; members can potentially have multiple registered cards
	DateStarted       time.Time         `json:"startDate"`
	DateEnded         time.Time         `json:"endDate,omitempty"`
}

// ContactPreference contains a list of contact information
type ContactPreference struct {
	Preference PrimaryContact `json:"preference"`
	Email      string         `json:"email"`
	Discord    string         `json:"discord,omitempty"` // discord user absolute ID (optional)
}

// EntryPreference defines whether the LEDs and sign should activate for this member
// and if so, what they should look like
// colour is an RGB hex value
type EntryPreference struct {
	Enabled         bool   `json:"enabled"`                  // whether or not any fanfare plays at all
	SignActive      bool   `json:"signActive,omitempty"`     // whether or not the sign announces their name
	ShowPronouns    bool   `json:"showPronouns,omitempty"`   // whether or not the sign announces their pronouns
	ShowName        bool   `json:"showName,omitempty"`       // default shows Username, but member can opt to show preferred "normal" name
	PubliclyLogged  bool   `json:"publiclyLogged,omitempty"` // whether or not the member's entry is logged to Discord
	NameColour      string `json:"colour,omitempty"`         // favourite colour that will play on LEDs
	DoorColour      string `json:"doorcColour,omitempty"`    // colour that the door arch will fade to
	PreferredSongID int    `json:"preferredSongID"`          // for playing a jingle on the door buzzer
	Emoji           string `json:"emoji,omitempty"`          // the member's favourite emoji
}

// PhysicalCard defines the properties of a physical card to be used for door entry
type PhysicalCard struct {
	ID       string `json:"id"`                 // the important bit: the actual ID of the card
	Type     int    `json:"type,omitempty"`     // type of the card (optional)
	BaudRate int    `json:"baudRate,omitempty"` // baud rate of the card (optional)
}

// GetMembersDatabase retrieves and decodes the database file from JSON
func GetMembersDatabase(logger zerolog.Logger, config *DoorbotConfig) (*MembersDB, error) {
	members := &MembersDB{}

	membersFile, err := os.Open(config.Paths.MemberDBFile)
	if err != nil {
		return nil, err
	}
	defer membersFile.Close()

	decoder := json.NewDecoder(membersFile)
	decoder.DisallowUnknownFields() // ensure no junk data is in the config file
	if err := decoder.Decode(members); err != nil {
		return nil, err
	}

	return members, nil
}

// CompareCardToMembers is the important bit of code here
// it iterates over the entire membership DB to see if a card is registered with any user
func (m *MembersDB) CompareCardToMembers(logger *zerolog.Logger, card PhysicalCard) (*Member, error) {
	logger.Info().Str("cardID", card.ID).Msg("comparing swiped card to members DB")

	var err error
	for _, memberDetails := range m.Members {
		for _, memberCard := range memberDetails.PhysicalCard {
			if memberCard.ID == card.ID {
				logger.Info().Str("cardID", card.ID).Str("member", memberDetails.Username).Msg("found matching card")
				if memberDetails.Active == false {
					err = fmt.Errorf("member not active [ID: %d, Username: %s]", memberDetails.ID, memberDetails.Username)
					logger.Error().Str("cardID", card.ID).Str("memberCard", memberCard.ID).Msg("member not active, or card assigned to wrong member")
				} else {
					return &memberDetails, nil
				}
			}
		}
	}

	// in case an error occurred, print that instead
	if err != nil {
		return nil, err
	}

	return nil, fmt.Errorf("member not found with card ID: %s", card.ID)
}
