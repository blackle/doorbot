# Wiring Guide for Doorbot

The NFC module we use is the [PN7150 V1](https://www.elechouse.com/product/pn7150_v1/) by Elechouse. It's an I2C device but also has a few other pins needed as well.

The pinout used is the same one provided by Elechouse:

(NFC Module -> RPi)

* SDA -> SDA (I2C Data)
* SCL -> SCL (I2C Clock)
* IRQ -> GPIO 23 (Interrupt)
* VEN -> GPIO 24 (Enable)
* VDD -> 3.3V (Main Power)
* VANT -> 5V (Antenna Power)
* GND -> GND (Ground)

Additional pins should be connected for all the other devices:

RGB LED Ring:

* +5V -> 5V
* Data IN -> GPIO 10 (MOSI)
* GND -> GND

Motion Sensor:

* VDD -> 5V
* SIG -> GPIO 26 
* GND -> GND

Doorbell Switch:

* Pin 1 -> GPIO 5
* Pin 2 -> 3.3V

Door Open Detector:

* Pin 1 -> GPIO 4
* Pin 2 -> 3.3V

Buzzer:

* Red Wire -> GPIO 18
* Black Wire -> GND

Event Mode Switch:

* Pin 1 -> GPIO 16
* Pin 2 -> 3.3V
