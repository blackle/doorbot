package services

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"slices"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/rs/zerolog"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	gonet "github.com/shirou/gopsutil/net"
	"gitlab.com/queer-computer-club/doorbot/config"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

const (
	discordComponent = "discord"

	// command names
	openDoorCmd = "open-door"
	pingCmd     = "ping"
	snapshotCmd = "snapshot"
	statusCmd   = "status"
	helpCmd     = "help"

	// a list of settings that only directors/ops can use
	settingsTopCmd          = "settings"
	settingsBuzzerCmd       = "buzzer"
	settingsOpenDetectorCmd = "open-detector"
	settingsReloadMembers   = "reload-members"

	// for determining public IP address
	publicIPServiceURL = "https://api.ipify.org?format=text"
)

// DiscordBotService contains everything needed to run a discord bot for doorbot
type DiscordBotService struct {
	context context.Context
	logger  zerolog.Logger
	cfg     *config.DoorbotConfig
	secrets *config.SecretsConfig
	members *config.MembersDB

	// the actual session used to connect to Discord
	session *discordgo.Session

	// channel used for the remote open functionality
	DiscordOpenChan chan bool
	// channel used to reload members.conf
	DiscordReloadMembers chan bool

	// pointer to current frames of camera
	// used for the "get current picture" command
	CurrentCameraFramePtr *[]byte
}

// NewDiscordBotService initializes the discord bot and allows commands to be added to the bot
func newDiscordBotService(ctx context.Context, l zerolog.Logger, cfg *config.DoorbotConfig, secrets *config.SecretsConfig, membersDB *config.MembersDB, cameraFramePtr *[]byte, membersReloadChan, doorOpenChan chan bool) (*DiscordBotService, error) {
	controller := &DiscordBotService{
		context:               ctx,
		CurrentCameraFramePtr: cameraFramePtr,
		secrets:               secrets,
		cfg:                   cfg,
		members:               membersDB,
	}

	logger := l.With().Str("channel-id", controller.secrets.Discord.ChannelID).Str("guild-id", controller.secrets.Discord.GuildID).Str("sub-component", discordComponent).Logger()

	controller.logger = logger

	// initialize session with Discord
	session, err := discordgo.New("Bot " + secrets.Discord.BotToken)
	if err != nil {
		return controller, err
	}

	// sets up what resources the session is supposed to access
	session.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildMessages | discordgo.IntentsMessageContent

	// open the session to discord
	if err := session.Open(); err != nil {
		return controller, err
	}

	controller.DiscordOpenChan = doorOpenChan
	controller.DiscordReloadMembers = membersReloadChan
	controller.session = session

	// below is discordgo magic
	commands, commandHandlers := controller.getCommandHandlers()

	controller.session.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if handler, ok := commandHandlers[i.ApplicationCommandData().Name]; ok {
			handler(s, i)
		}
	})

	for _, handler := range commands {
		_, err := controller.session.ApplicationCommandCreate(controller.session.State.User.ID, controller.secrets.Discord.GuildID, handler)
		if err != nil {
			logger.Error().Err(err).Msg("could not create command handler for discord")
		}
	}

	_, err = controller.session.ChannelMessageSend(secrets.Discord.ChannelID, fmt.Sprintf("Doorbot %s online!", config.GetVersion()))
	if err != nil {
		logger.Error().Err(err).Msg("could not send join message")
	}

	if err := controller.session.UpdateWatchStatus(0, "epic win"); err != nil {
		logger.Error().Err(err).Msg("could not set status")
	}

	logger.Info().Msg("initialized discord service")

	return controller, nil
}

// Start begins the loop that accepts commands for the service
func (dsc *DiscordBotService) Start(waitGroup *sync.WaitGroup) {
	if dsc.session == nil {
		dsc.logger.Debug().Msg("skipping discord service")
		return
	}

	dsc.logger.Debug().Msg("starting discord controller")
	go dsc.discordLoop(waitGroup)
	dsc.logger.Info().Msg("started discord controller")
}

// main discord loop (unused, but could be at some point)
func (dsc *DiscordBotService) discordLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	dsc.logger.Debug().Msg("beginning discord loop")

	for {
		select {
		case <-dsc.context.Done():
			dsc.logger.Debug().Msg("stopping discord controller")
			dsc.PostBasic("Doorbot shutting down!")
			dsc.removeAllHandlers()
			dsc.session.Close()
			dsc.logger.Info().Msg("stopped discord controller")
			waitGroup.Done()
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}

// PostBasic can post a variety of messages to discord
func (dsc *DiscordBotService) PostBasic(post string) {
	if dsc.session == nil {
		dsc.logger.Debug().Msg("session is nil; skipping discord service")
		return
	}

	// post to discord
	_, err := dsc.session.ChannelMessageSend(dsc.secrets.Discord.ChannelID, post)
	if err != nil {
		dsc.logger.Error().Err(err).Msg("could not post to discord")
		return
	}

	dsc.logger.Info().Str("post", post).Msg("posted message to discord")
}

// checkUserID ensures that the user's ID on Discord is correct
func (dsc *DiscordBotService) checkUserID(configDiscordID string) (string, error) {
	if dsc.session == nil {
		return "", fmt.Errorf("session is nil; skipping discord service")
	}

	dsc.logger.Debug().Str("id", configDiscordID).Msg("getting username from ID")

	guildMembers, err := dsc.session.GuildMembers(dsc.secrets.Discord.GuildID, "0", 1000)
	if err != nil {
		return "", fmt.Errorf("could not get guild data from Discord: %s", err.Error())
	}

	for _, member := range guildMembers {
		dsc.logger.Trace().Str("discord-username", member.User.Username).Str("discord-member-id", member.User.ID).Msg("members list")
		if member.User.ID == configDiscordID {
			dsc.logger.Debug().Str("id", configDiscordID).Str("discord-username", member.User.Username).Str("discord-member-id", member.User.ID).Msg("member ID found")
			return member.User.Username, nil
		}
	}

	return "", fmt.Errorf("could not get member ID")
}

// PostWithImage posts to Discord with an image attachment
func (dsc *DiscordBotService) PostWithImage(message string, image []byte) {
	if dsc.session == nil {
		dsc.logger.Debug().Msg("session is nil; skipping discord service")
		return
	}

	dsc.logger.Debug().Msg("posting to discord with image")

	embed := &discordgo.MessageEmbed{
		Title:       "Doorbot Bell Notification",
		Description: "Ding dong! Doorbell has been rung!",
		Color:       0xc378de,
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "If you recognize this person, use the following command:",
				Value:  fmt.Sprintf("/%s", openDoorCmd),
				Inline: true,
			},
		},
	}

	post := &discordgo.MessageSend{}

	var reader *bytes.Reader
	if len(image) > 0 {
		reader = bytes.NewReader(image)
	} else {
		b, err := base64.StdEncoding.DecodeString(notFoundImage)
		if err != nil {
			dsc.logger.Error().Err(err).Msg("could not decode image")
		}
		reader = bytes.NewReader(b)
		dsc.logger.Warn().Msg("no image data found from camera; appstopping default")
	}

	dsc.logger.Debug().Msg("appstopping image data for discord post")
	post.Files = []*discordgo.File{{Name: "image.jpg", ContentType: "image/jpeg", Reader: reader}}
	embed.Image = &discordgo.MessageEmbedImage{URL: "attachment://image.jpg"}

	post.Embeds = []*discordgo.MessageEmbed{embed}

	_, err := dsc.session.ChannelMessageSendComplex(dsc.secrets.Discord.ChannelID, post)
	if err != nil {
		dsc.logger.Error().Err(err).Msg("could not post message to discord")
	}

	dsc.logger.Info().Msg("posted with image to discord")
}

// PostMemberEntryMessage posts an entry message whenever a member swipes their card on the door
// the "id" is their QCC Member ID
func (dsc *DiscordBotService) PostMemberEntryMessage(id int) {
	var member *config.Member
	for _, memberInList := range dsc.members.Members {
		if memberInList.ID == id {
			member = &memberInList
			dsc.logger.Debug().Str("member-name", member.Username).Int("id", id).Msg("found member for entry message post")
			break
		}
	}
	if member == nil {
		return
	}
	// discord will only @-mention someone if you get their numerical ID (i.e: "1234567890")
	// so: try to get the numerical ID, but if it doesn't work, fall back to standard username
	dsc.logger.Debug().Int("member-id", id).Str("member", member.Username).Str("discord-id", member.ContactPreference.Discord).Msg("posting to discord")

	var mention string
	// make sure the user exists at Discord based on the ID provided
	discordMemberName, err := dsc.checkUserID(member.ContactPreference.Discord)
	if err != nil {
		dsc.logger.Error().Err(err).Str("discord-username", discordMemberName).Str("username", member.ContactPreference.Discord).Msg("falling back to username")
		mention = fmt.Sprintf("%s", member.ContactPreference.Discord)
	} else {
		// @-mentions for bots need to be decorated with <@{ID}>
		mention = fmt.Sprintf("<@%s>", member.ContactPreference.Discord)
	}

	var post string
	if member.EntryPreference.ShowName {
		post = fmt.Sprintf(defaultMemberEntryMessage, member.Name, mention)
	} else {
		post = fmt.Sprintf(defaultMemberEntryMessage, member.Username, mention)
	}

	dsc.PostBasic(post)
}

// checkIfUserHasRole checks to see if the requesting user has a particular error
// if not, return an error
func checkIfUserHasRole(s *discordgo.Session, i *discordgo.InteractionCreate, guildID string, roleIDs []string) error {
	roles, err := s.GuildRoles(guildID)
	if err != nil {
		return fmt.Errorf("could not get guild roles: %w", err)
	}

	// get the role ID for "Members"
	var memberRoleID string
	for _, guildRole := range roles {
		if slices.Contains(roleIDs, guildRole.ID) {
			memberRoleID = guildRole.ID
		}
	}

	// check all the roles the member has
	for _, roleID := range i.Member.Roles {
		if roleID == memberRoleID {
			// if the user has the Member role, return nil, which indicates that the user is authorized
			return nil
		}
	}

	return fmt.Errorf("command refused (unauthorized)")
}

// getCommandHandlers defines up the slash commands that the bot will use
// "open door remotely", etc.
func (dsc *DiscordBotService) getCommandHandlers() ([]*discordgo.ApplicationCommand, map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)) {
	// specify the command description here
	commands := []*discordgo.ApplicationCommand{
		{
			Name:        pingCmd,
			Description: "Ping doorbot!",
		},
		{
			Name:        statusCmd,
			Description: "Print Doorbot status, including CPU/Mem stats, IP address, etc.",
		},
		{
			Name:        openDoorCmd,
			Description: "Open the main door to the space.",
		},
		{
			Name:        snapshotCmd,
			Description: "Get a live picture from the doorbot doorcam.",
		},
		{
			Name:        helpCmd,
			Description: "Print a help message on how to use Doorbot.",
		},
		{
			Name:        settingsTopCmd,
			Description: "(Directors Only) Toggle various settings on doorbot while running.",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:        settingsBuzzerCmd,
					Description: "Toggle the buzzer on/off.",
					Type:        discordgo.ApplicationCommandOptionSubCommand,
				},
				{
					Name:        settingsOpenDetectorCmd,
					Description: "Toggle the door open detector on/off.",
					Type:        discordgo.ApplicationCommandOptionSubCommand,
				},
				{
					Name:        settingsReloadMembers,
					Description: "Reload the membership file.",
					Type:        discordgo.ApplicationCommandOptionSubCommand,
				},
			},
		},
	}

	// write what the command does here, using the same "Name" from above
	// NOTE: you MUST use "checkIfUserHasRole"
	commandHandlers := map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
		// "open-door" opens the door remotely
		openDoorCmd: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			requestingMember := i.Member.User.Username
			logger := dsc.logger.With().Str("command", openDoorCmd).Str("requesting-member", requestingMember).Logger()

			logger.Debug().Msg("open-door command")

			// IMPORTANT: ensure that the user is a "Member", as only members may use these commands
			if err := checkIfUserHasRole(s, i, dsc.secrets.Discord.GuildID, dsc.secrets.Discord.MemberRoleIDs); err != nil {
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: err.Error(),
					},
				})

				logger.Error().Err(err).Msg("error processing request")
				return
			}

			// signal to open the door remotely
			dsc.DiscordOpenChan <- true

			// return a nice response
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Door open!",
				},
			})

			logger.Info().Msg("door opened for member on discord")
		},
		// "ping" is a simple ping command
		pingCmd: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			requestingMember := i.Member.User.Username
			logger := dsc.logger.With().Str("command", pingCmd).Str("requesting-member", requestingMember).Logger()
			logger.Debug().Msg("ping command")

			// IMPORTANT: ensure that the user is a "Member", as only members may use these commands
			if err := checkIfUserHasRole(s, i, dsc.secrets.Discord.GuildID, dsc.secrets.Discord.MemberRoleIDs); err != nil {
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: err.Error(),
					},
				})

				logger.Error().Err(err).Msg("error processing request")
				return
			}

			// return a response
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Pong!",
				},
			})

			logger.Info().Msg("sent response to discord")
		},
		// "help" shows a help message
		helpCmd: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			requestingMember := i.Member.User.Username
			logger := dsc.logger.With().Str("command", helpCmd).Str("requesting-member", requestingMember).Logger()
			logger.Debug().Msg("help command")

			// IMPORTANT: ensure that the user is a "Member", as only members may use these commands
			if err := checkIfUserHasRole(s, i, dsc.secrets.Discord.GuildID, dsc.secrets.Discord.MemberRoleIDs); err != nil {
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: err.Error(),
					},
				})

				logger.Error().Err(err).Msg("error processing request")
				return
			}

			// return a response
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "How to use Doorbot:\n" +
						"TODO" +
						"",
				},
			})

			logger.Info().Msg("sent response to discord")
		},
		// "settings" lets directors/ops toggle options remotely
		settingsTopCmd: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			requestingMember := i.Member.User.Username
			logger := dsc.logger.With().Str("command", settingsTopCmd).Str("requesting-member", requestingMember).Logger()
			logger.Debug().Msg("settings command")

			// IMPORTANT: ensure that the user is a "Directors", as only directors may use this command
			if err := checkIfUserHasRole(s, i, dsc.secrets.Discord.GuildID, []string{dsc.secrets.Discord.DirectorRoleID}); err != nil {
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: err.Error(),
					},
				})

				logger.Error().Err(err).Msg("error processing request")
				return
			}

			options := i.ApplicationCommandData().Options
			content := ""

			switch options[0].Name {
			case settingsTopCmd:
				content = "Use one of the toggles. i.e: `/settings buzzer` to toggle the buzzer on/off."
			case settingsBuzzerCmd:
				dsc.cfg.Buzzer.Enabled = !dsc.cfg.Buzzer.Enabled
				content = fmt.Sprintf("Toggled buzzer! Status: %v", dsc.convertBoolToReadable(dsc.cfg.Buzzer.Enabled))
			case settingsOpenDetectorCmd:
				dsc.cfg.Misc.DoorOpenEnabled = !dsc.cfg.Misc.DoorOpenEnabled
				content = fmt.Sprintf("Toggled door open detector! Status: %v", dsc.convertBoolToReadable(dsc.cfg.Misc.DoorOpenEnabled))
			case settingsReloadMembers:
				dsc.DiscordReloadMembers <- true
				content = "Reloading member's list!"
			}

			// return a response
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: content,
				},
			})

			logger.Info().Msg("sent response to discord")
		},
		// "snapshot" sends a snapshot from the webcam on demand
		snapshotCmd: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			requestingMember := i.Member.User.Username
			logger := dsc.logger.With().Str("command", snapshotCmd).Str("requesting-member", requestingMember).Logger()

			logger.Debug().Msg("picture command")

			// IMPORTANT: ensure that the user is a "Member", as only members may use these commands
			if err := checkIfUserHasRole(s, i, dsc.secrets.Discord.GuildID, dsc.secrets.Discord.MemberRoleIDs); err != nil {
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: err.Error(),
					},
				})

				logger.Error().Err(err).Msg("error processing request")
				return
			}

			// "embeds" produce a nice-looking coloured box
			embed := &discordgo.MessageEmbed{
				Title:       "Doorbot Doorcam Snapshot",
				Description: "Here's what I'm currently seeing:",
				Color:       0xc378de,
			}

			logger.Debug().Int("image-size", len(*dsc.CurrentCameraFramePtr)).Msg("appstopping image data for snapshot post")

			// this appends the image to the post within the embed
			// this is a two step process: you have to upload the image as a File, and then reference it within the Embed
			// this makes the image appear only once
			var reader *bytes.Reader
			if len(*dsc.CurrentCameraFramePtr) > 0 {
				reader = bytes.NewReader(*dsc.CurrentCameraFramePtr)
			} else {
				b, err := base64.StdEncoding.DecodeString(notFoundImage)
				if err != nil {
					logger.Error().Err(err).Msg("could not decode image")
				}
				reader = bytes.NewReader(b)
				logger.Warn().Msg("no image data found from camera; appstopping default")
			}

			responseData := &discordgo.InteractionResponseData{}
			responseData.Files = []*discordgo.File{{Name: "image.jpg", ContentType: "image/jpeg", Reader: reader}}
			embed.Image = &discordgo.MessageEmbedImage{URL: "attachment://image.jpg"}
			responseData.Embeds = []*discordgo.MessageEmbed{embed}

			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: responseData,
			})

			logger.Info().Msg("posted current snapshot from webcam to discord")
		},
		// "status" prints out system stats for the machine running Doorbot
		"status": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			requestingMember := i.Member.User.Username
			logger := dsc.logger.With().Str("command", statusCmd).Str("requesting-member", requestingMember).Logger()

			logger.Debug().Msg("status command")

			// IMPORTANT: ensure that the user is a "Member", as only members may use these commands
			if err := checkIfUserHasRole(s, i, dsc.secrets.Discord.GuildID, dsc.secrets.Discord.MemberRoleIDs); err != nil {
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: err.Error(),
					},
				})

				logger.Error().Err(err).Msg("error processing request")
				return
			}

			// load average of CPU
			la, err := load.AvgWithContext(context.Background())
			if err != nil {
				logger.Error().Err(err).Msg("could not get load average stats")
				return
			}
			loadAvg := fmt.Sprintf("%.2f/%.2f/%.2f", la.Load1, la.Load5, la.Load15)

			// memory used
			mem, err := mem.VirtualMemory()
			if err != nil {
				logger.Error().Err(err).Msg("could not get memory stats")
				return
			}
			memString := fmt.Sprintf("%.2f%%", mem.UsedPercent)

			// generic host info (OS, uptime)
			host, err := host.Info()
			if err != nil {
				logger.Error().Err(err).Msg("could not get host info")
				return
			}
			osString := fmt.Sprintf("%s %s", cases.Title(language.Und, cases.NoLower).String(host.Platform), host.PlatformVersion)
			uptimeString := fmt.Sprint(time.Duration(host.Uptime * 1e9))

			// network interfaces (IP address)
			localNetInfo, err := gonet.Interfaces()
			if err != nil {
				logger.Error().Err(err).Msg("could not get local network info")
				return
			}

			// on a Raspberry Pi, they typically get 3 interfaces: localhost, ethernet, and WiFi
			// first, we'll check ethernet [1], but if that doesn't exist, check wifi [2] instead
			// there may also be other network interfaces so go up to 5 just in case
			localIPString := "unknown"
			for i := 1; i <= 5; i++ {
				if len(localNetInfo[i].Addrs) > 0 {
					localIPString = localNetInfo[i].Addrs[0].Addr
					break
				}
			}

			// get public IP too
			publicIPString := "unknown"
			resp, err := http.Get(publicIPServiceURL)
			if err != nil {
				logger.Error().Err(err).Msg("could not get public IP")
			} else {
				ip, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					logger.Error().Err(err).Msg("could not read public IP")
				} else {
					publicIPString = string(ip)
				}

			}
			defer resp.Body.Close()

			// once again, we want to use a nicely-formatted embed
			// the "fields" can be arranged inline for a tighter-looking response inside Discord
			embed := &discordgo.MessageEmbed{
				Title: "Doorbot System Status",
				Color: 0x769b76,
				Fields: []*discordgo.MessageEmbedField{
					{
						Name:   "Hostname",
						Value:  host.Hostname,
						Inline: true,
					},
					{
						Name:   "OS",
						Value:  osString,
						Inline: true,
					},
					{
						Name:   "Uptime",
						Value:  uptimeString,
						Inline: true,
					},
					{
						Name:   "CPU Load",
						Value:  loadAvg,
						Inline: true,
					},
					{
						Name:   "Memory",
						Value:  memString,
						Inline: true,
					},
					{
						Name:   "Local IP",
						Value:  localIPString,
						Inline: true,
					},
					{
						Name:   "Public IP",
						Value:  publicIPString,
						Inline: true,
					},
					{
						Name:   "Version",
						Value:  config.GetVersion(),
						Inline: true,
					},
				},
			}

			responseData := &discordgo.InteractionResponseData{}
			responseData.Embeds = []*discordgo.MessageEmbed{embed}

			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: responseData,
			})

			logger.Info().Msg("posted machine stats to discord")
		},
	}

	return commands, commandHandlers
}

func (dsc *DiscordBotService) convertBoolToReadable(b bool) string {
	if b {
		return "On"
	}
	return "Off"
}

// removeAllHandlers removes all slash commands from the bot upon shutdown
func (dsc *DiscordBotService) removeAllHandlers() {
	registeredCommands, err := dsc.session.ApplicationCommands(dsc.session.State.User.ID, dsc.secrets.Discord.GuildID)
	if err != nil {
		dsc.logger.Error().Err(err).Msg("could not get registered commands for removal; skipping")
		return
	}

	for _, c := range registeredCommands {
		dsc.logger.Trace().Str("command", c.Name).Msg("deleting command on discord")
		if err := dsc.session.ApplicationCommandDelete(dsc.session.State.User.ID, dsc.secrets.Discord.GuildID, c.ID); err != nil {
			dsc.logger.Error().Err(err).Str("command", c.Name).Msg("could not delete registered command")
		}
	}
}
