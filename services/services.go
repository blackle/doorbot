package services

import (
	"context"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/queer-computer-club/doorbot/config"
)

const (
	serviceManagerComponent   = "service-manager"
	defaultMemberEntryMessage = "Member %s (%s) has entered the space!"
)

// ServiceManager manages any messaging services doorbot might be connected to (Discord, IRC, etc)
type ServiceManager struct {
	context           context.Context
	logger            zerolog.Logger
	Connections       []Service
	ReloadMembersChan chan bool // for reloading the members list
	DoorOpenChan      chan bool // for signalling to open the door
}

// Service interface contains a list that every messaging service handler should have
// this allows us to initialize all messaging services the same way, and put them in a list
type Service interface {
	Start(*sync.WaitGroup)
	PostBasic(string)
	PostWithImage(string, []byte)
	PostMemberEntryMessage(int)
}

// NewServiceManager brings up all services that have been configured
// Add new ones here as handlers are created
func NewServiceManager(ctx context.Context, l zerolog.Logger, cfg *config.DoorbotConfig, secrets *config.SecretsConfig, members *config.MembersDB, cameraFramePtr *[]byte) (*ServiceManager, error) {
	logger := l.With().Str("component", serviceManagerComponent).Logger()

	serviceManager := &ServiceManager{
		context:           ctx,
		logger:            logger,
		ReloadMembersChan: make(chan bool, 2),
		DoorOpenChan:      make(chan bool, 2),
	}

	var err error
	if secrets.Discord.Enabled {
		discordBot, err := newDiscordBotService(ctx, logger, cfg, secrets, members, cameraFramePtr, serviceManager.ReloadMembersChan, serviceManager.DoorOpenChan)
		if err == nil {
			serviceManager.Connections = append(serviceManager.Connections, discordBot)
		}
	}

	return serviceManager, err
}

// Start starts all configured services
func (s *ServiceManager) Start(wg *sync.WaitGroup) {
	if len(s.Connections) <= 0 {
		s.logger.Trace().Msg("no service manager connections found; skipping")
		return
	}
	go s.serviceLoop(wg)
	for _, service := range s.Connections {
		service.Start(wg)
	}
}

// PostBasic posts a text message to all configured services
func (s *ServiceManager) PostBasic(post string) {
	if len(s.Connections) <= 0 {
		s.logger.Trace().Msg("no service manager connections found; skipping")
		return
	}
	s.logger.Debug().Msg("posting basic message")
	for _, service := range s.Connections {
		service.PostBasic(post)
	}
}

// PostWithImage posts with an image to all configured services
func (s *ServiceManager) PostWithImage(post string, image []byte) {
	if len(s.Connections) <= 0 {
		s.logger.Trace().Msg("no service manager connections found; skipping")
		return
	}
	s.logger.Debug().Msg("posting message with image")
	for _, service := range s.Connections {
		service.PostWithImage(post, image)
	}
}

// PostMemberEntryMessage posts an entry message based on the memberID that triggered the event
// i.e: when a card is swiped against the reader
func (s *ServiceManager) PostMemberEntryMessage(memberID int) {
	if len(s.Connections) <= 0 {
		s.logger.Trace().Msg("no service manager connections found; skipping")
		return
	}
	s.logger.Debug().Msg("posting member entry message")
	for _, service := range s.Connections {
		service.PostMemberEntryMessage(memberID)
	}
}

// main service manager loop
func (s *ServiceManager) serviceLoop(waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)
	s.logger.Debug().Msg("starting service manager loop")

	for {
		select {
		case <-s.context.Done():
			s.logger.Debug().Msg("stopping service manager")
			s.logger.Info().Msg("stopped service manager")
			waitGroup.Done()
			return

		case <-time.After(10 * time.Millisecond):
			continue
		}
	}
}
