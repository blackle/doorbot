# **************** #
# doorbot makefile #
# **************** #

# shamelessly stolen from: https://dev.to/eugenebabichenko/generating-pretty-version-strings-including-nightly-with-git-and-makefiles-48p3
TAG_COMMIT := $(shell git rev-list --abbrev-commit --tags --max-count=1)
VERSION := $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null || true)
COMMIT := $(shell git rev-parse --short HEAD)
DATE := $(shell git log -1 --format=%cd --date=format:"%Y%m%d")

ifneq ($(COMMIT), $(TAG_COMMIT))
ifneq ($(strip $(VERSION)),)
	VERSION := $(VERSION)-dev-$(DATE)-$(COMMIT)
endif
endif

ifeq ($(strip $(VERSION)),)
	VERSION := $(COMMIT)-$(DATE)
endif

ifneq ($(shell git status --porcelain),)
	VERSION := $(VERSION)-dev
endif


FLAGS := -ldflags "-X gitlab.com/queer-computer-club/doorbot/config.DoorbotVersion=$(VERSION)"

help: ## Show this help dialog
	@echo "\nDoorbot Makefile Help\n\n\"epic win\"\n\nBelow are the commands you can use when developing or deploying Doorbot!\n"
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-].*+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Build
build: clean ## Compile doorbot
	$(info variable version = $(VERSION))
	go build $(FLAGS) -o build/doorbot gitlab.com/queer-computer-club/doorbot

clean: ## Delete the compiled doorbot binary
	rm -f build/*

##@ Tests
test: ## Run tests for doorbot
	go test -v ./...

##@ Run
run: ## Run doorbot
	sudo build/doorbot -config etc/settings.conf

run/debug: ## Run doorbot with debug logging
	sudo build/doorbot -debug -config etc/settings.conf

run/trace: ## Run doorbot with trace logging (more logs)
	sudo build/doorbot -trace -config etc/settings.conf

run/fake: ## Run doorbot with a fake NFC module, so it can be run without hardware attached
	sudo build/doorbot -debug -config etc/settings.conf -fake-nfc

##@ Deploy
install: test build ## Install doorbot to /usr/local/bin
	sudo systemctl stop doorbot
	sudo cp build/doorbot /usr/local/bin/doorbot
	sudo systemctl start doorbot

install-config: install ## Install configs and systemd service file
	sudo mkdir -p /etc/doorbot
	for file in settings members songs secrets; do \
		test -f /etc/doorbot/$$file.conf || { sudo rsync --chown=root:sudo --chmod=0660 etc/$$file.conf /etc/doorbot/$$file.conf; }; \
	done
	sudo touch /var/log/doorbot.log && sudo chmod 660 /var/log/doorbot.log
	sudo cp etc/doorbot.service /lib/systemd/system
	sudo systemctl daemon-reload
	sudo systemctl enable doorbot

.PHONY: help clean test run run/dummy run/trace install install-config
